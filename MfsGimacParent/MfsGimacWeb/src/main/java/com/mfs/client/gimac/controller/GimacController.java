package com.mfs.client.gimac.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.gimac.dto.AccountIncomingRequestDto;
import com.mfs.client.gimac.dto.AccountIncomingResponseDto;
import com.mfs.client.gimac.dto.AuthenticationResponseDto;
import com.mfs.client.gimac.dto.ListPaymentInquiryResponseDto;
import com.mfs.client.gimac.dto.PaymentInquiryRequestDto;
import com.mfs.client.gimac.dto.RemittanceCancellationRequestDto;
import com.mfs.client.gimac.dto.RemittanceCancellationResponseDto;
import com.mfs.client.gimac.dto.WalletIncomingCancellationRequestDto;
import com.mfs.client.gimac.dto.WalletIncomingCancellationResponseDto;
import com.mfs.client.gimac.dto.WalletIncomingRequestDto;
import com.mfs.client.gimac.dto.WalletIncomingResponseDto;
import com.mfs.client.gimac.service.AccountIncomingRemittanceService;
import com.mfs.client.gimac.service.AuthenticationService;
import com.mfs.client.gimac.service.IncomingRemittanceCancellationService;
import com.mfs.client.gimac.service.PaymentInquiryService;
import com.mfs.client.gimac.service.WalletIncomingCancellationService;
import com.mfs.client.gimac.service.WalletIncomingRemittanceService;
import com.mfs.client.gimac.util.CommonValidations;
import com.mfs.client.gimac.util.MFSGimacCodes;

@RestController
public class GimacController {

	@Autowired
	AccountIncomingRemittanceService accountIncomingRemittanceService;

	@Autowired
	IncomingRemittanceCancellationService incomingRemittanceCancellationService;

	@Autowired
	AuthenticationService authenticationService;

	@Autowired
	PaymentInquiryService paymentInquiryService;

	@Autowired
	WalletIncomingRemittanceService walletIncomingService;

	@Autowired
	WalletIncomingCancellationService walletCancellationService;
	
	private static final Logger LOGGER = Logger.getLogger(GimacController.class);

	@GetMapping(value = "/auth")
	public AuthenticationResponseDto authentication() {
		AuthenticationResponseDto response = new AuthenticationResponseDto();
		response = authenticationService.authenticationService();
		return response;
	}

	@PostMapping(value = "/accountRemittance")
	public AccountIncomingResponseDto accountIncomingRemittanceRequest(@RequestBody AccountIncomingRequestDto request) {
		LOGGER.debug("GimacController in accountIncomingRemittanceRequest function " + request);
		AccountIncomingResponseDto response = new AccountIncomingResponseDto();
		CommonValidations commonValidations = new CommonValidations();
		if (request != null) {

			if (commonValidations.validateStringValues(request.getIssuerTrxRef())) {
				response = new AccountIncomingResponseDto();
				response.setCode(MFSGimacCodes.INVALID_ISSUERTRXREF.getCode());
				response.setMessage(MFSGimacCodes.INVALID_ISSUERTRXREF.getMessage());

			} else if (!commonValidations.validateString(request.getIntent())) {
				response = new AccountIncomingResponseDto();
				response.setCode(MFSGimacCodes.INVALID_INTENT.getCode());
				response.setMessage(MFSGimacCodes.INVALID_INTENT.getMessage());

			} else if (commonValidations.validateStringValues(request.getAccountNumber())) {
				response = new AccountIncomingResponseDto();
				response.setCode(MFSGimacCodes.INVALID_ACCOUNT_NUMBER.getCode());
				response.setMessage(MFSGimacCodes.INVALID_ACCOUNT_NUMBER.getMessage());

			} else if (commonValidations.validateStringValues(request.getToMember())) {
				response = new AccountIncomingResponseDto();
				response.setCode(MFSGimacCodes.INVALID_TOMEMBER.getCode());
				response.setMessage(MFSGimacCodes.INVALID_TOMEMBER.getMessage());

			} else if (commonValidations.validateAmountValues(request.getAmount()) || (request.getAmount() <= 0)) {

				response.setCode(MFSGimacCodes.INVALID_AMOUNT.getCode());
				response.setMessage(MFSGimacCodes.INVALID_AMOUNT.getMessage());

			} else if (commonValidations.validateNumber(request.getCurrency())) {
				response = new AccountIncomingResponseDto();
				response.setCode(MFSGimacCodes.INVALID_CURRENCY.getCode());
				response.setMessage(MFSGimacCodes.INVALID_CURRENCY.getMessage());

			} else {

				response = accountIncomingRemittanceService.accountIncomingRemittance(request);
			}
		} else {

			response = new AccountIncomingResponseDto();
			response.setCode(MFSGimacCodes.NULL_REQUEST.getCode());
			response.setMessage(MFSGimacCodes.NULL_REQUEST.getMessage());

		}
		return response;

	}

	@PostMapping(value = "/remittanceCancelltion")
	public RemittanceCancellationResponseDto remittanceCancellation(
			@RequestBody RemittanceCancellationRequestDto request) {
		LOGGER.debug("GimacController in remittanceCancellation function " + request);

		RemittanceCancellationResponseDto response = new RemittanceCancellationResponseDto();
		CommonValidations commonValidations = new CommonValidations();
		if (request != null) {

			if (commonValidations.validateStringValues(request.getIssuerTrxRef())) {

				response = new RemittanceCancellationResponseDto();
				response.setCode(MFSGimacCodes.INVALID_ISSUERTRXREF.getCode());
				response.setMessage(MFSGimacCodes.INVALID_ISSUERTRXREF.getMessage());

			} else if (!commonValidations.validateString(request.getIntent())) {

				response = new RemittanceCancellationResponseDto();
				response.setCode(MFSGimacCodes.INVALID_INTENT.getCode());
				response.setMessage(MFSGimacCodes.INVALID_INTENT.getMessage());

			} else if (commonValidations.validateNumber(request.getVoucherCode())) {

				response = new RemittanceCancellationResponseDto();
				response.setCode(MFSGimacCodes.INVALID_VOUCHER_CODE.getCode());
				response.setMessage(MFSGimacCodes.INVALID_VOUCHER_CODE.getMessage());

			} else if (commonValidations.isStringOnlyAlphabet(request.getState())) {

				response = new RemittanceCancellationResponseDto();
				response.setCode(MFSGimacCodes.INVALID_STATE.getCode());
				response.setMessage(MFSGimacCodes.INVALID_STATE.getMessage());

			} else if (commonValidations.validateStringValues(request.getUpdateTime())) {

				response = new RemittanceCancellationResponseDto();
				response.setCode(MFSGimacCodes.INVALID_UPDATE_TIME.getCode());
				response.setMessage(MFSGimacCodes.INVALID_UPDATE_TIME.getMessage());

			} else {

				response = incomingRemittanceCancellationService.incomingRemittanceCancellation(request);

			}
		} else {
			response = new RemittanceCancellationResponseDto();
			response.setCode(MFSGimacCodes.NULL_REQUEST.getCode());
			response.setMessage(MFSGimacCodes.NULL_REQUEST.getMessage());
		}
		return response;
	}


	@PostMapping(value = "/walletRemittance")
	public WalletIncomingResponseDto walletIncominResponse(@RequestBody WalletIncomingRequestDto request) {
		LOGGER.debug("GimacController in walletIncominResponse function " + request);

		WalletIncomingResponseDto response = new WalletIncomingResponseDto();

		CommonValidations commonValidations = new CommonValidations();
		if (request != null) {

			if (commonValidations.validateStringValues(request.getIssuerTrxRef())) {
				response.setCode(MFSGimacCodes.INVALID_ISSUERTRXREF.getCode());
				response.setMessage(MFSGimacCodes.INVALID_ISSUERTRXREF.getMessage());

			} else if (!commonValidations.validateString(request.getIntent())) {

				response.setCode(MFSGimacCodes.INVALID_INTENT.getCode());
				response.setMessage(MFSGimacCodes.INVALID_INTENT.getMessage());
			} else if (commonValidations.validateStringValues(request.getWalletDestination())) {

				response.setCode(MFSGimacCodes.INVALID_WALLETDESTINATION.getCode());
				response.setMessage(MFSGimacCodes.INVALID_WALLETDESTINATION.getMessage());

			} else if (commonValidations.validateStringValues(request.getToMember())) {
				response.setCode(MFSGimacCodes.INVALID_TOMEMBER.getCode());
				response.setMessage(MFSGimacCodes.INVALID_TOMEMBER.getMessage());

			} else if (commonValidations.validateAmountValues(request.getAmount()) || (request.getAmount() <= 0)) {

				response.setCode(MFSGimacCodes.INVALID_AMOUNT.getCode());
				response.setMessage(MFSGimacCodes.INVALID_AMOUNT.getMessage());

			} else if (commonValidations.validateNumber(request.getCurrency())) {
				response.setCode(MFSGimacCodes.INVALID_CURRENCY.getCode());
				response.setMessage(MFSGimacCodes.INVALID_CURRENCY.getMessage());

			} else {

				response = walletIncomingService.walletIncomingService(request);
			}
		} else {

			response.setCode(MFSGimacCodes.NULL_REQUEST.getCode());
			response.setMessage(MFSGimacCodes.NULL_REQUEST.getMessage());

		}
		return response;

	}

	@PostMapping(value = "/walletCancellation")
	public WalletIncomingCancellationResponseDto walletCancellation(
			@RequestBody WalletIncomingCancellationRequestDto request) {
		LOGGER.debug("GimacController in walletCancellation function " + request);

		WalletIncomingCancellationResponseDto response = new WalletIncomingCancellationResponseDto();
		CommonValidations commonValidations = new CommonValidations();
		if (request != null) {

			if (commonValidations.validateStringValues(request.getIssuerTrxRef())) {

				response.setCode(MFSGimacCodes.INVALID_ISSUERTRXREF.getCode());
				response.setMessage(MFSGimacCodes.INVALID_ISSUERTRXREF.getMessage());

			}else if (!commonValidations.validateString(request.getIntent())) {
				response.setCode(MFSGimacCodes.INVALID_INTENT.getCode());
				response.setMessage(MFSGimacCodes.INVALID_INTENT.getMessage());

			} else if (commonValidations.validateNumber(request.getVoucherCode())) {

				response.setCode(MFSGimacCodes.INVALID_VOUCHER_CODE.getCode());
				response.setMessage(MFSGimacCodes.INVALID_VOUCHER_CODE.getMessage());

			} else if (commonValidations.isStringOnlyAlphabet(request.getState())) {

				response.setCode(MFSGimacCodes.INVALID_STATE.getCode());
				response.setMessage(MFSGimacCodes.INVALID_STATE.getMessage());

			} else if (commonValidations.validateStringValues(request.getUpdateTime())) {

				response.setCode(MFSGimacCodes.INVALID_UPDATE_TIME.getCode());
				response.setMessage(MFSGimacCodes.INVALID_UPDATE_TIME.getMessage());

			} else {

				response = walletCancellationService.walletCancellation(request);
			}
		} else {

			response.setCode(MFSGimacCodes.NULL_REQUEST.getCode());
			response.setMessage(MFSGimacCodes.NULL_REQUEST.getMessage());
		}
		return response;
	}
	
	@PostMapping(value = "/paymentInquiry")
	public ListPaymentInquiryResponseDto paymentInquiry(@RequestBody PaymentInquiryRequestDto request) {
		LOGGER.debug("GimacController in paymentInquiry function " + request);

		CommonValidations commonValidations = new CommonValidations();
		ListPaymentInquiryResponseDto response = new ListPaymentInquiryResponseDto();
		if (request != null) {
			if (commonValidations.validateStringValues(request.getIssuerTrxRef())) {

				response.setCode(MFSGimacCodes.INVALID_ISSUERTRXREF.getCode());
				response.setMessage(MFSGimacCodes.INVALID_ISSUERTRXREF.getMessage());
			} else {
				response = paymentInquiryService.paymentInquiryService(request);
			}
		} else {

			response = new ListPaymentInquiryResponseDto();
			response.setCode(MFSGimacCodes.NULL_REQUEST.getCode());
			response.setMessage(MFSGimacCodes.NULL_REQUEST.getMessage());

		}

		return response;
	}

}