package com.mfs.client.gimac.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.gimac.dto.WalletIncomingCancellationRequestDto;
import com.mfs.client.gimac.dto.WalletIncomingCancellationResponseDto;
import com.mfs.client.gimac.service.WalletIncomingCancellationService;
/*
* @author Priyanka Prakash Khade
* WalletIncomingCancellationServiceTest.java
* 26-05-2020
*
* purpose: this class is used for WalletIncomingCancellationService Test
*
* Methods
* 1.walletCancellationSuccessTest
* 2.walletCancellationFailTest
*/
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class WalletIncomingCancellationServiceTest {

	@Autowired
	WalletIncomingCancellationService walletCancellationService;

	// Success test
	@Ignore
	@Test
	public void walletCancellationSuccessTest() {

		WalletIncomingCancellationRequestDto request = new WalletIncomingCancellationRequestDto();

		request.setIssuerTrxRef("w1");
		request.setIntent("inc_wal_remit");
		request.setUpdateTime("1490109044");
		request.setVoucherCode("123456789012");
		request.setState("PENDING");
		
		WalletIncomingCancellationResponseDto response = walletCancellationService.walletCancellation(request);
		Assert.assertEquals("w1", response.getIssuerTrxRef());
	}

	// Fail test
    @Ignore
	@Test
	public void walletCancellationFailTest() {

		WalletIncomingCancellationRequestDto request = new WalletIncomingCancellationRequestDto();

		request.setIssuerTrxRef("1");
		request.setIntent("inc_wal_remit");
		request.setUpdateTime("1490109044");
		request.setVoucherCode("123456789012");
		request.setState("PENDING");
		WalletIncomingCancellationResponseDto response = walletCancellationService.walletCancellation(request);
		String a = response.getError();
		Assert.assertEquals("400", response.getError());

	}

}
