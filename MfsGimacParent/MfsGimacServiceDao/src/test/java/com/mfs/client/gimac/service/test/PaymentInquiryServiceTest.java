package com.mfs.client.gimac.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.gimac.dto.ListPaymentInquiryResponseDto;
import com.mfs.client.gimac.dto.PaymentInquiryRequestDto;
import com.mfs.client.gimac.service.PaymentInquiryService;
/*
* @author Dhanashree Ingle
* PaymentInquiryServiceTest.java
* 26-05-2020
*
* purpose: this class is used for PaymentInquiryServiceTest
*
* 
*
*/
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class PaymentInquiryServiceTest {
	
	@Autowired
	PaymentInquiryService paymentInquiryService;
	
	//Test for Success
	@Ignore
	@Test
	public void paymentInquiryServiceSuccessTest() {
		
		PaymentInquiryRequestDto request=new PaymentInquiryRequestDto();
		List<String> ACCEPTED=new ArrayList<String>();
		List<String> atm_transfer=new ArrayList<String>();
		request.setAsIssuer("true");
		request.setAsAcquirer("false");
		request.setFirstElement("1");
		request.setPageSize("2");
		request.setAcquirerTrxRef("3");
		request.setFromMember("4");
		request.setToMember("4");
		request.setFromDate("5");
		request.setToDate("6");
		request.setIssuerTrxRef("77");
		request.setVoucherCode("1111111111");
		request.setStatus(ACCEPTED);
		request.setIntents(atm_transfer);
		ListPaymentInquiryResponseDto response=paymentInquiryService.paymentInquiryService(request);
		Assert.assertEquals(2, response.getPaymentInquiry().size());
		System.out.println(response);
	}
	
	//Test for Fail
	@Ignore
	@Test
	public void paymentInquiryServiceFailTest() {
		PaymentInquiryRequestDto request=new PaymentInquiryRequestDto();
		List<String> ACCEPTED=new ArrayList<String>();
		List<String> atm_transfer=new ArrayList<String>();
		request.setAsIssuer("true");
		request.setAsAcquirer("false");
		request.setFirstElement("1");
		request.setPageSize("2");
		request.setAcquirerTrxRef("3");
		request.setFromMember("4");
		request.setToMember("4");
		request.setFromDate("5");
		request.setToDate("6");
		request.setIssuerTrxRef("77");
		request.setVoucherCode("1111111111");
		request.setStatus(ACCEPTED);
		request.setIntents(atm_transfer);
		ListPaymentInquiryResponseDto response=paymentInquiryService.paymentInquiryService(request);
		String a = response.getError();
		Assert.assertEquals("400",response.getError());
	}
	}