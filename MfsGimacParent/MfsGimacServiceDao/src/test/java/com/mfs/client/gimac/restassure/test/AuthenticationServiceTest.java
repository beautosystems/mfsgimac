package com.mfs.client.gimac.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import com.jayway.restassured.http.ContentType;
/**
 * 
 * @author pallavi
 *  AuthorizationServiceTest.java 
 *  27--2020
 * 
 * purpose : this class is used for test authorization(using rest assure) 
 * 
 * Methods 
 * 1.authorizationServiceTest
 * 2.authorizationServiceNotNullTest
 * 3.authorizationServiceFailTest
 * 
 * Change_history
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthenticationServiceTest {
	
	//function to test success
		@Ignore
		@Test
		public void authorizationServiceTest() {

			given().contentType(ContentType.JSON).when().get("http://localhost:8080/MfsGimacWeb/auth")
					.then().statusCode(equalTo(HttpStatus.OK.value())).body("token_type", is("bearer"));
		}

		//function to test not null 
		@Ignore
		@Test
		public void authorizationServiceNotNullTest() {

			given().contentType(ContentType.JSON).when().get("http://localhost:8080/MfsGimacWeb/auth")
					.then().statusCode(equalTo(HttpStatus.OK.value())).body("token_type", not(nullValue()));
		}

		//function to test fail
		@Ignore
		@Test
		public void authorizationServiceFailTest() {

			given().contentType(ContentType.JSON).when().get("http://localhost:8080/MfsGimacWeb/auth")
					.then().statusCode(equalTo(HttpStatus.OK.value())).body("code", is("ERROR_CODE"));
		}
	
	
}
