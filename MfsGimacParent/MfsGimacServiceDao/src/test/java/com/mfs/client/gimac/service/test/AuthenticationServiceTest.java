package com.mfs.client.gimac.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.gimac.dto.AuthenticationResponseDto;
import com.mfs.client.gimac.service.AuthenticationService;
/**
 * 
 * @author pallavi
 *  AuthorizationServiceTest.java 
 *  27--2020
 * 
 * purpose : this class is used for test authorization(using) 
 * 
 * Methods 
 * 1.authorizationServiceTest
 * 2.authorizationServiceFailTest
 * 
 * Change_history
 */
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthenticationServiceTest {
	@Autowired
	AuthenticationService authenticationService;

	// function to test success
	@Ignore
	@Test
	public void authorizationServiceTest() {
		AuthenticationResponseDto response=authenticationService.authenticationService();
		System.out.println(response);
		Assert.assertEquals("access_token",response.getAccess_token());

	}
	
	// function to test fail
		@Ignore
		@Test
		public void authorizationServiceFailTest() {
			AuthenticationResponseDto response=authenticationService.authenticationService();
			System.out.println(response);
			Assert.assertEquals("ERROR_CODE",response.getCode());

		}

}
