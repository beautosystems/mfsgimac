package com.mfs.client.gimac.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.gimac.dto.AccountIncomingRequestDto;
import com.mfs.client.gimac.dto.AccountIncomingResponseDto;
import com.mfs.client.gimac.dto.ReceiverCustomerDataDto;
import com.mfs.client.gimac.dto.RemittanceCancellationRequestDto;
import com.mfs.client.gimac.dto.RemittanceCancellationResponseDto;
import com.mfs.client.gimac.dto.SenderCustomerDataDto;
import com.mfs.client.gimac.service.IncomingRemittanceCancellationService;
/*
* @author Arnav Gharote
* IncomingRemittanceCancellationServiceTest.java
* 25-05-2020
*
* purpose:this class is used for IncomingRemittanceCancellationServiceTest
*
*/
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class RemittanceCancellationServiceTest {

	@Autowired
	IncomingRemittanceCancellationService incomingRemittanceCancellationService;

	// Success test
	@Ignore
	@Test
	public void remittanceCancellationSuccessTest() {

		RemittanceCancellationRequestDto request = new RemittanceCancellationRequestDto();

		request.setIssuerTrxRef("op6");
		request.setIntent("inc_wal_remit");
		request.setUpdateTime("1490109044");
		request.setVoucherCode("123456789012");
		request.setState("CANCELLED");

		RemittanceCancellationResponseDto response = incomingRemittanceCancellationService
				.incomingRemittanceCancellation(request);
		Assert.assertEquals("op6", response.getIssuerTrxRef());
	}

	// Fail test
	@Ignore
	@Test
	public void remittanceCancellationFailTest() {

		RemittanceCancellationRequestDto request = new RemittanceCancellationRequestDto();

		request.setIssuerTrxRef("op6");
		request.setIntent("inc_wal_remit");
		request.setUpdateTime("1490109044");
		request.setVoucherCode("123456789012");
		request.setState("PENDING");

		RemittanceCancellationResponseDto response = incomingRemittanceCancellationService
				.incomingRemittanceCancellation(request);
		String a = response.getError();
		Assert.assertEquals("400", response.getError());

	}

}
