package com.mfs.client.gimac.restassure.test;
import static com.jayway.restassured.RestAssured.given;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.gimac.dto.AccountIncomingRequestDto;
import com.mfs.client.gimac.dto.ReceiverCustomerDataDto;
import com.mfs.client.gimac.dto.SenderCustomerDataDto;
import com.mfs.client.gimac.service.AccountIncomingRemittanceService;
/*
* @author Arnav Gharote
* AccountIncomingRemittanceServiceRestassureTest.java
* 25-05-2020
*
* purpose:this class is used for AccountIncomingRemittanceRestAssureServiceTest
*
*/
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class accountIncomingRemittanceRestAssureTest {
	
	@Autowired
	AccountIncomingRemittanceService accountIncomingRemittanceService;

	@Test
	public void accountIncomingRemittanceTest() {
		
		AccountIncomingRequestDto request=new AccountIncomingRequestDto();
		SenderCustomerDataDto senderData=new SenderCustomerDataDto();
		ReceiverCustomerDataDto receiverData=new ReceiverCustomerDataDto();
		
		request.setIssuerTrxRef("9");
		request.setIntent("inc_acc_remit");
		request.setCreateTime("1490109044");
		request.setSenderMobile("+212522564541");
		request.setAccountNumber("1234567890123456");
		request.setToMember("XXXXXXXX");
		request.setDescription("It is description");
		request.setAmount(123);
		request.setCurrency("950");
		senderData.setFirstName("Hassan");
		senderData.setSecondName("TYPE");
		senderData.setIdType("national id");
		senderData.setIdNumber("BE145278");
		senderData.setAddress("Road 2,Casablanca");
		senderData.setBirthDate("21/09/1981");
		receiverData.setFirstName("Ali");
		receiverData.setSecondName("BAGHO");
		receiverData.setIdType("passport");
		receiverData.setIdNumber("XC145278");
		receiverData.setAddress("Road3,Casablanca");
		receiverData.setCity("Casablanca");
		receiverData.setCountry("Morocco");
		receiverData.setPhone("+2125224578");
		receiverData.setBirthDate("21/09/1981");
		request.setSenderCustomerData(senderData);
		request.setReceiverCustomerData(receiverData);
		
		System.out.println(request);
		
		given().body(request).contentType(ContentType.JSON).when()
		.post("http://localhost:8082/MfsGimacWeb/accountRemittance").then().statusCode(equalTo(HttpStatus.OK.value()))
		.body("issuerTrxRef", is("9"));
	}

}
