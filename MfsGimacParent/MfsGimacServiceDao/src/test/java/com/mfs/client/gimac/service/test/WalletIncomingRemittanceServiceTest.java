package com.mfs.client.gimac.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.gimac.dto.ReceiverCustomerDataDto;
import com.mfs.client.gimac.dto.SenderCustomerDataDto;
import com.mfs.client.gimac.dto.WalletIncomingRequestDto;
import com.mfs.client.gimac.dto.WalletIncomingResponseDto;
import com.mfs.client.gimac.service.WalletIncomingRemittanceService;

/*
* @author Priyanka Prakash Khade
* WalletIncomingRemittanceServiceTest.java
* 25-05-2020
*
* purpose: this class is used for WalletIncomingRemittanceService Test
*
* Methods
* 1.walletIncomingRemittanceSuccessTest
* 2.accountIncomingRemittanceFailTest()
*/
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class WalletIncomingRemittanceServiceTest {

	@Autowired
	WalletIncomingRemittanceService walletIncomingRemittanceService;

	// Success test
	@Ignore
	@Test
	public void walletIncomingRemittanceSuccessTest() {

		WalletIncomingRequestDto request = new WalletIncomingRequestDto();
		SenderCustomerDataDto senderData = new SenderCustomerDataDto();
		ReceiverCustomerDataDto receiverData = new ReceiverCustomerDataDto();

		request.setIssuerTrxRef("w1");
		request.setIntent("inc_wal_remit");
		request.setCreateTime("1490109044");
		request.setSenderMobile("+212522564541");
		request.setWalletDestination("1234567890123456");
		request.setToMember("XXXXXXXX");
		request.setDescription("It is description");
		request.setAmount(5000);
		request.setCurrency("950");
		senderData.setFirstName("Hassan");
		senderData.setSecondName("TYPE");
		senderData.setIdType("national id");
		senderData.setIdNumber("BE145278");
		senderData.setAddress("Road 2,Casablanca");
		senderData.setBirthDate("21/09/1981");
		receiverData.setFirstName("Ali");
		receiverData.setSecondName("BAGHO");
		receiverData.setIdType("passport");
		receiverData.setIdNumber("XC145278");
		receiverData.setAddress("Road3,Casablanca");
		receiverData.setCity("Casablanca");
		receiverData.setCountry("Morocco");
		receiverData.setPhone("+2125224578");
		receiverData.setBirthDate("21/09/1981");
		request.setSenderCustomerData(senderData);
		request.setReceiverCustomerData(receiverData);

		WalletIncomingResponseDto response = walletIncomingRemittanceService.walletIncomingService(request);
		System.out.println(response);
		Assert.assertEquals("w1", response.getIssuerTrxRef());

	}

	// Fail test
	@Ignore
	@Test
	public void accountIncomingRemittanceFailTest() {

		WalletIncomingRequestDto request = new WalletIncomingRequestDto();
		SenderCustomerDataDto senderData = new SenderCustomerDataDto();
		ReceiverCustomerDataDto receiverData = new ReceiverCustomerDataDto();

		request.setIssuerTrxRef("11");
		request.setIntent("inc_acc_remit");
		request.setCreateTime("1490109044");
		request.setSenderMobile("+212522564541");
		request.setWalletDestination("1234567890123456");
		request.setToMember("XXXXXXXX");
		request.setDescription("It is description");
		request.setAmount(123);
		request.setCurrency("950");
		senderData.setFirstName("Hassan");
		senderData.setSecondName("TYPE");
		senderData.setIdType("national id");
		senderData.setIdNumber("BE145278");
		senderData.setAddress("Road 2,Casablanca");
		senderData.setBirthDate("21/09/1981");
		receiverData.setFirstName("Ali");
		receiverData.setSecondName("BAGHO");
		receiverData.setIdType("passport");
		receiverData.setIdNumber("XC145278");
		receiverData.setAddress("Road3,Casablanca");
		receiverData.setCity("Casablanca");
		receiverData.setCountry("Morocco");
		receiverData.setPhone("+2125224578");
		receiverData.setBirthDate("21/09/1981");
		request.setSenderCustomerData(senderData);
		request.setReceiverCustomerData(receiverData);

		WalletIncomingResponseDto response = walletIncomingRemittanceService.walletIncomingService(request);
		System.out.println(response);
		String a = response.getError();
		System.out.println(a);
		Assert.assertEquals("400", response.getError());

	}

}
