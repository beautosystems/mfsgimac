package com.mfs.client.gimac.dao.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.gimac.dao.TransactionDao;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacTransactionLog;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class TransactionDaoTest {

	@Autowired
	TransactionDao transactionDao;

	//Test for success
	@Test
	@Ignore
	public void getTransactionByIssuerTrxRefTest() throws DaoException {
		GimacTransactionLog gtLog = new GimacTransactionLog();
		gtLog = transactionDao.getTransactionByIssuerTrxRef("2");
		Assert.assertNotNull(gtLog);
		System.out.println(gtLog);
	}

}