package com.mfs.client.gimac.restassure.test;
/*
* @author Dhanashree Ingle
* PaymentInquiryServiceRestassureTest.java
* 26-05-2020
*
* purpose:this class is used for PaymentInquiryServiceRestassureTest
*
*/
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.gimac.dto.ListPaymentInquiryResponseDto;
import com.mfs.client.gimac.dto.PaymentInquiryRequestDto;
import com.mfs.client.gimac.service.PaymentInquiryService;
@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class PaymentInquiryRestAssureTest {
	
	@Autowired
	PaymentInquiryService  paymentInquiryService;
	
	@Test
    public void paymentInquiryServiceRestAssure() {
		
		PaymentInquiryRequestDto request=new PaymentInquiryRequestDto();
		List<String> ACCEPTED=new ArrayList<String>();
		List<String> atm_transfer=new ArrayList<String>();
		request.setAsIssuer("");
		request.setAsAcquirer("");
		request.setFirstElement("");
		request.setPageSize("");
		request.setAcquirerTrxRef("");
		request.setVoucherCode("");
		request.setFromMember("");
		request.setToMember("");
		request.setFromDate("");
		request.setToDate("");
		request.setIssuerTrxRef("1111111116771");
		request.setVoucherCode("AB345EF89012");
		request.setStatus(ACCEPTED);
		request.setIntents(atm_transfer);
		System.out.println(request);
		
		given().body(request).contentType(ContentType.JSON).when()
		.post("http://localhost:8081/MfsGimacWeb/paymentInquiry").then().statusCode(equalTo(HttpStatus.OK.value()))
		.body("issuerTrxRef", is("1111111116771"));
}
	
}