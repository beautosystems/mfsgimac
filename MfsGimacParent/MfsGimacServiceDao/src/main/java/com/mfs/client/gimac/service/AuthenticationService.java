package com.mfs.client.gimac.service;

import com.mfs.client.gimac.dto.AuthenticationResponseDto;

public interface AuthenticationService {
	public AuthenticationResponseDto authenticationService();
}
