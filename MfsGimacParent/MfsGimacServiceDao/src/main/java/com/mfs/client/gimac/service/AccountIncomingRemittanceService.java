package com.mfs.client.gimac.service;

import com.mfs.client.gimac.dto.AccountIncomingRequestDto;
import com.mfs.client.gimac.dto.AccountIncomingResponseDto;

public interface AccountIncomingRemittanceService {
	
	AccountIncomingResponseDto accountIncomingRemittance(AccountIncomingRequestDto request);


}
