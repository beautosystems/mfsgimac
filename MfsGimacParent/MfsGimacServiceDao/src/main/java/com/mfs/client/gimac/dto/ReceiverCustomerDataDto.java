package com.mfs.client.gimac.dto;

public class ReceiverCustomerDataDto {

	private String firstName;
	private String secondName;
	private String idType;
	private String idNumber;
	private String address;
	private String city;
	private String country;
	private String phone;
	private String birthDate;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	@Override
	public String toString() {
		return "ReceiverCustomerDataDto [firstName=" + firstName + ", secondName=" + secondName + ", idType=" + idType
				+ ", idNumber=" + idNumber + ", address=" + address + ", city=" + city + ", country=" + country
				+ ", phone=" + phone + ", birthDate=" + birthDate + "]";
	}


}
