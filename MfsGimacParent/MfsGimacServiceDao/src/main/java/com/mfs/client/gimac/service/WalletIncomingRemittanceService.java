package com.mfs.client.gimac.service;

import com.mfs.client.gimac.dto.WalletIncomingRequestDto;
import com.mfs.client.gimac.dto.WalletIncomingResponseDto;

public interface WalletIncomingRemittanceService {

	WalletIncomingResponseDto walletIncomingService (WalletIncomingRequestDto request);
}
