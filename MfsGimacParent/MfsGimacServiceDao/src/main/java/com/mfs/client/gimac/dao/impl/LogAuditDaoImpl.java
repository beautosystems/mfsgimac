package com.mfs.client.gimac.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.gimac.dao.LogAuditDao;
import com.mfs.client.gimac.models.GimacAudit;

@EnableTransactionManagement
@Repository("LogAuditDao")
public class LogAuditDaoImpl implements LogAuditDao {
	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(LogAuditDaoImpl.class);

	@Transactional
	public int logAuditDetails(GimacAudit audit) {

		int transactionAuditId = 0;

		LOGGER.debug("Inside logAuditDetails of LogAuditDaoImpl");
		Session session = sessionFactory.getCurrentSession();
		transactionAuditId = (Integer) session.save(audit);
		LOGGER.info("auditSave Save " + transactionAuditId);
		return transactionAuditId;

	}

	
}
