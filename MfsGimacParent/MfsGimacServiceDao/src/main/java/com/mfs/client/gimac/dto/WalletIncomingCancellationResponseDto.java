package com.mfs.client.gimac.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class WalletIncomingCancellationResponseDto {

	private String error;
	private String errorDescription;
	private String issuerTrxRef;
	private String voucherCode;
	private String state;
	private String updateTime;
	private String code;
	private String message;

	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}

	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "WalletIncomingCancellationResponseDto [error=" + error + ", errorDescription=" + errorDescription
				+ ", issuerTrxRef=" + issuerTrxRef + ", voucherCode=" + voucherCode + ", state=" + state
				+ ", updateTime=" + updateTime + ", code=" + code + ", message=" + message + "]";
	}

}
