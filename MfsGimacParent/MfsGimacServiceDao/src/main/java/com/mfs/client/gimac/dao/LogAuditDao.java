package com.mfs.client.gimac.dao;

import com.mfs.client.gimac.models.GimacAudit;

public interface LogAuditDao {

	public int logAuditDetails(GimacAudit audit);

}
