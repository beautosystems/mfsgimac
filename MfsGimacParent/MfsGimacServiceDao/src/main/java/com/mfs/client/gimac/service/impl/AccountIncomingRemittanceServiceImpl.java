package com.mfs.client.gimac.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.gimac.dao.SystemConfigDao;
import com.mfs.client.gimac.dao.TransactionDao;
import com.mfs.client.gimac.dto.AccountIncomingRequestDto;
import com.mfs.client.gimac.dto.AccountIncomingResponseDto;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacAuthentication;
import com.mfs.client.gimac.models.GimacEventLog;
import com.mfs.client.gimac.models.GimacTransactionError;
import com.mfs.client.gimac.models.GimacTransactionLog;
import com.mfs.client.gimac.service.AccountIncomingRemittanceService;
import com.mfs.client.gimac.util.CallServices;
import com.mfs.client.gimac.util.CommonConstant;
import com.mfs.client.gimac.util.ExpiredToken;
import com.mfs.client.gimac.util.HttpsConnectionRequest;
import com.mfs.client.gimac.util.HttpsConnectionResponse;
import com.mfs.client.gimac.util.HttpsConnectorImpl;
import com.mfs.client.gimac.util.JSONToObjectConversion;
import com.mfs.client.gimac.util.MFSGimacCodes;

/*
* @author Arnav Gharote
* AccountIncomingRemittanceService.java
* 25-05-2020
*
* purpose: this class is used for AccountIncomingRemittanceService
*
* Methods
* 1. accountIncomingRemittance
* 2.log request
* 3.log request in audit table
* 2.log response
* 3.log error request and response
*
*/
@Service("AccountIncomingRemittanceService")
public class AccountIncomingRemittanceServiceImpl extends BaseAuditServiceImpl
		implements AccountIncomingRemittanceService {

	private static final Logger LOGGER = Logger.getLogger(AccountIncomingRemittanceServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDao systemConfigDao;

	public AccountIncomingResponseDto accountIncomingRemittance(AccountIncomingRequestDto request) {

		Map<String, String> headerMap = new HashMap<String, String>();
		Gson gson = new Gson();
		AccountIncomingResponseDto response = null;
		String serviceResponse = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();

		try {

			// Log request in audit table
			logAuditDetails(request.getIssuerTrxRef(), request.getIntent(), request.getSenderMobile(),
					request.getAccountNumber(), request.getToMember(), request.getAmount(), request.getCurrency(),
					request.getSenderCustomerData().getFirstName(), request.getSenderCustomerData().getSecondName(),
					request.getSenderCustomerData().getIdNumber(), request.getReceiverCustomerData().getFirstName(),
					request.getReceiverCustomerData().getSecondName(), request.getReceiverCustomerData().getIdNumber(),
					"null", request.getReceiverCustomerData().getPhone(), "null", new Date(),
					CommonConstant.ACCOUNT_INCOMING_REMITTANCE_SERVICE, CommonConstant.INITIATE_REQUEST, "null");

			// check for duplicate IssuerTrxRef id
			GimacTransactionLog transctionLog = transactionDao.getTransactionByIssuerTrxRef(request.getIssuerTrxRef());
			if (transctionLog == null) {

				// get last generated Token ID
				GimacAuthentication accessToken = transactionDao.getLastRecordByAccessTokenId();
				if (accessToken != null) {

					// get Config Details
					Map<String, String> configMap = systemConfigDao.getConfigDetailsMap();

					// log request
					logRequest(request);

					// set service URL
					connectionRequest.setHttpmethodName("POST");
					if (configMap.get(CommonConstant.PORT) != null) {
						connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
					}
					connectionRequest.setServiceUrl(configMap.get(CommonConstant.BASE_URL)
							+ configMap.get(CommonConstant.PAYMENT) + "/" + configMap.get(CommonConstant.SEND));

					headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + accessToken.getAccessToken());

					connectionRequest.setHeaders(headerMap);
					String req = gson.toJson(request);

					// eventLog
					eventRequestLog(req, request.getIssuerTrxRef());

					// log detais in audit table
					logAuditDetails(request.getIssuerTrxRef(), request.getIntent(), request.getSenderMobile(),
							request.getAccountNumber(), request.getToMember(), request.getAmount(),
							request.getCurrency(), request.getSenderCustomerData().getFirstName(),
							request.getSenderCustomerData().getSecondName(),
							request.getSenderCustomerData().getIdNumber(),
							request.getReceiverCustomerData().getFirstName(),
							request.getReceiverCustomerData().getSecondName(),
							request.getReceiverCustomerData().getIdNumber(), "null",
							request.getReceiverCustomerData().getPhone(), "null", new Date(),
							CommonConstant.ACCOUNT_INCOMING_REMITTANCE_SERVICE,
							CommonConstant.INITIATE_REQUEST_TO_PARTNER, "null");

					LOGGER.info(
							"AccountIncomingRemittanceServiceImpl in accountIncomingRemittance function sending transfer request with MfsId:  "
									+ req);
					// getting gimac response
					// serviceResponse
					// =CallServices.getResponseFromService(connectionRequest,req);
					LOGGER.info(
							"AccountIncomingRemittanceServiceImpl in accountIncomingRemittanceService function received transfer response for MfsId: "
									+ serviceResponse);
					serviceResponse = "{\"issuerTrxRef\":\"" + request.getIssuerTrxRef() + "\"," + "\"intent\":\""
							+ request.getIntent() + "\",\"createTime\":\"" + request.getCreateTime() + "\","
							+ "\"senderMobile\":\"" + request.getSenderMobile() + "\",\"accountNumber\":\""
							+ request.getAccountNumber() + "\"," + "\"toMember\":\"" + request.getToMember()
							+ "\",\"description\":\"" + request.getDescription() + "\",\"amount\":"
							+ request.getAmount() + "," + "\"currency\":\"" + request.getCurrency()
							+ "\",\"state\":\"PENDING\",\"voucherCode\":\"123456789012\","
							+ "\"acquirerTrxRef\":\"0909\",\"senderCustomerData\":{\"firstName\":\""
							+ request.getSenderCustomerData().getFirstName() + "\"," + "\"secondName\":\""
							+ request.getSenderCustomerData().getSecondName() + "\",\"idType\":\""
							+ request.getSenderCustomerData().getIdType() + "\",\"idNumber\":\""
							+ request.getSenderCustomerData().getIdNumber() + "\"," + "\"address\":\""
							+ request.getSenderCustomerData().getAddress() + "\",\"birthDate\":\""
							+ request.getSenderCustomerData().getBirthDate() + "\"},"
							+ "\"receiverCustomerData\":{\"firstName\":\"Ali\",\"secondName\":\" BAGHO\","
							+ "\"idType\":\"passport\",\"idNumber\":\"XC145278\",\"address\":\"Road3,Casablanca\","
							+ "\"city\":\"Casablanca\",\"country\":\"Morocco\",\"phone\":\"+2125224578\","
							+ "\"birthDate\":\"21-09-1981\"}}";

					eventResponseLog(serviceResponse, request.getIssuerTrxRef(),
							CommonConstant.ACCOUNT_INCOMING_REMITTANCE_SERVICE);

					response = (AccountIncomingResponseDto) JSONToObjectConversion.getObjectFromJson(serviceResponse,
							AccountIncomingResponseDto.class);

					// Check for success and fail
					if (response == null) {
						// Null response
						response = new AccountIncomingResponseDto();
						response.setCode(MFSGimacCodes.ER201.getCode());
						response.setMessage(MFSGimacCodes.ER201.getMessage());

					} else if (response.getError() != null) {

						errorLogResponse(request, response);

					} else {

						logResponse(request, response);

					}

				} else {
					// null access token
					response = new AccountIncomingResponseDto();
					response.setCode(MFSGimacCodes.ER216.getCode());
					response.setMessage(MFSGimacCodes.ER216.getMessage());
				}
			} else {
				// Duplicate IssuerTrxRef id
				response = new AccountIncomingResponseDto();
				response.setCode(MFSGimacCodes.ER215.getCode());
				response.setMessage(MFSGimacCodes.ER215.getMessage());
			}
			// Audit request and response
			logAuditDetails(request.getIssuerTrxRef(), request.getIntent(), request.getSenderMobile(),
					request.getAccountNumber(), request.getToMember(), request.getAmount(), request.getCurrency(),
					request.getSenderCustomerData().getFirstName(), request.getSenderCustomerData().getSecondName(),
					request.getSenderCustomerData().getIdNumber(), request.getReceiverCustomerData().getFirstName(),
					request.getReceiverCustomerData().getSecondName(), request.getReceiverCustomerData().getIdNumber(),
					request.getReceiverCustomerData().getPhone(), response.getState(), response.getVoucherCode(),
					new Date(), CommonConstant.ACCOUNT_INCOMING_REMITTANCE_SERVICE,
					CommonConstant.RESPONSE_FROM_PARTNER, response.getMessage());
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in AccountIncomingRemittanceServiceImpl" + de);
			response = new AccountIncomingResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in AccountIncomingRemittanceServiceImpl" + e);
			response = new AccountIncomingResponseDto();
			response.setCode(MFSGimacCodes.ER207.getCode());
			response.setMessage(MFSGimacCodes.ER207.getMessage());
		}
		return response;
	}

	// event response log
	private void eventResponseLog(String response, String issuerTrxRef, String servicename) throws DaoException {

		GimacEventLog transactionEventLog = transactionDao.getByIssureTrxRefAndServiceName(issuerTrxRef, servicename);

		if (transactionEventLog != null) {
			transactionEventLog.setResponse(response);
			transactionEventLog.setDateLogged(new Date());
			transactionDao.update(transactionEventLog);

		}
	}

	// event request log
	private void eventRequestLog(String request, String issuerTrxRef) throws DaoException {

		GimacEventLog eventLog = new GimacEventLog();

		eventLog.setMfsId(issuerTrxRef);
		eventLog.setRequest(request);
		eventLog.setServiceName(CommonConstant.ACCOUNT_INCOMING_REMITTANCE_SERVICE);
		eventLog.setDateLogged(new Date());
		transactionDao.save(eventLog);

	}

	// log error request response
	private void errorLogResponse(AccountIncomingRequestDto request, AccountIncomingResponseDto response)
			throws DaoException {

		GimacTransactionError transactionErrorLog = new GimacTransactionError();

		transactionErrorLog.setIssuerTrxRef(request.getIssuerTrxRef());
		transactionErrorLog.setIntent(request.getIntent());
		transactionErrorLog.setSenderMobile(request.getSenderMobile());
		transactionErrorLog.setAccountNumber(request.getAccountNumber());
		transactionErrorLog.setToMember(request.getToMember());
		transactionErrorLog.setAmount(request.getAmount());
		transactionErrorLog.setCurrency(request.getCurrency());
		transactionErrorLog.setSenderFirstName(request.getSenderCustomerData().getFirstName());
		transactionErrorLog.setSenderSecondName(request.getSenderCustomerData().getSecondName());
		transactionErrorLog.setSenderIdNumber(request.getSenderCustomerData().getIdNumber());
		transactionErrorLog.setReceiverFirstName(request.getReceiverCustomerData().getFirstName());
		transactionErrorLog.setReceiverSecondName(request.getReceiverCustomerData().getSecondName());
		transactionErrorLog.setReceiverIdNumber(request.getReceiverCustomerData().getIdNumber());
		transactionErrorLog.setState(response.getState());
		transactionErrorLog.setVoucherCode(response.getVoucherCode());
		transactionErrorLog.setAcquirerTrxRef(response.getAcquirerTrxRef());
		transactionErrorLog.setServiceName("AccountIncomingRemittance");
		transactionErrorLog.setDateLogged(new Date());
		transactionErrorLog.setError(response.getError());
		transactionErrorLog.setErrorDescription(response.getErrorDescription());
		transactionDao.save(transactionErrorLog);
	}

	// log response
	private void logResponse(AccountIncomingRequestDto request, AccountIncomingResponseDto response)
			throws DaoException {

		GimacTransactionLog transactionLog = transactionDao.getTransactionByIssuerTrxRef(request.getIssuerTrxRef());
		if (transactionLog != null) {
			transactionLog.setIntent(response.getIntent());
			transactionLog.setCreateTime(response.getCreateTime());
			transactionLog.setSenderMobile(response.getSenderMobile());
			transactionLog.setAccountNumber(response.getAccountNumber());
			transactionLog.setToMember(response.getToMember());
			transactionLog.setDescription(response.getDescription());
			transactionLog.setAmount(response.getAmount());
			transactionLog.setCurrency(response.getCurrency());
			transactionLog.setState(response.getState());
			transactionLog.setVoucherCode(response.getVoucherCode());
			transactionLog.setAcquirerTrxRef(response.getAcquirerTrxRef());
			transactionLog.setSenderFirstName(response.getSenderCustomerData().getFirstName());
			transactionLog.setSenderSecondName(response.getSenderCustomerData().getSecondName());
			transactionLog.setSenderIdType(response.getSenderCustomerData().getIdType());
			transactionLog.setSenderIdNumber(response.getSenderCustomerData().getIdNumber());
			transactionLog.setSenderAddress(response.getSenderCustomerData().getAddress());
			transactionLog.setSenderBirthDate(response.getSenderCustomerData().getBirthDate());
			transactionLog.setReceiverFirstName(response.getReceiverCustomerData().getFirstName());
			transactionLog.setReceiverSecondName(response.getReceiverCustomerData().getSecondName());
			transactionLog.setReceiverIdType(response.getReceiverCustomerData().getIdType());
			transactionLog.setReceiverIdNumber(response.getReceiverCustomerData().getIdNumber());
			transactionLog.setReceiverAddress(response.getReceiverCustomerData().getAddress());
			transactionLog.setReceiverCity(response.getReceiverCustomerData().getCity());
			transactionLog.setReceiverCountry(response.getReceiverCustomerData().getCountry());
			transactionLog.setReceiverPhone(response.getReceiverCustomerData().getPhone());
			transactionLog.setReceiverBirthDate(response.getReceiverCustomerData().getBirthDate());
			transactionLog.setServiceName("AccountIncomingRemittance");
			transactionLog.setError(response.getError());
			transactionLog.setErrorDescription(response.getErrorDescription());
			transactionLog.setDateLogged(new Date());
			transactionDao.update(transactionLog);
		}
	}

	// log request
	private void logRequest(AccountIncomingRequestDto request) throws DaoException {

		GimacTransactionLog transactionLog = new GimacTransactionLog();
		transactionLog.setIssuerTrxRef(request.getIssuerTrxRef());
		transactionLog.setIntent(request.getIntent());
		transactionLog.setCreateTime(request.getCreateTime());
		transactionLog.setSenderMobile(request.getSenderMobile());
		transactionLog.setAccountNumber(request.getAccountNumber());
		transactionLog.setToMember(request.getToMember());
		transactionLog.setDescription(request.getDescription());
		transactionLog.setAmount(request.getAmount());
		transactionLog.setCurrency(request.getCurrency());
		transactionLog.setSenderFirstName(request.getSenderCustomerData().getFirstName());
		transactionLog.setSenderSecondName(request.getSenderCustomerData().getSecondName());
		transactionLog.setSenderIdType(request.getSenderCustomerData().getIdType());
		transactionLog.setSenderIdNumber(request.getSenderCustomerData().getIdNumber());
		transactionLog.setSenderAddress(request.getSenderCustomerData().getAddress());
		transactionLog.setSenderBirthDate(request.getSenderCustomerData().getBirthDate());
		transactionLog.setReceiverFirstName(request.getReceiverCustomerData().getFirstName());
		transactionLog.setReceiverSecondName(request.getReceiverCustomerData().getSecondName());
		transactionLog.setReceiverIdType(request.getReceiverCustomerData().getIdType());
		transactionLog.setReceiverIdNumber(request.getReceiverCustomerData().getIdNumber());
		transactionLog.setReceiverAddress(request.getReceiverCustomerData().getAddress());
		transactionLog.setReceiverCity(request.getReceiverCustomerData().getCity());
		transactionLog.setReceiverCountry(request.getReceiverCustomerData().getCountry());
		transactionLog.setReceiverPhone(request.getReceiverCustomerData().getPhone());
		transactionLog.setReceiverBirthDate(request.getReceiverCustomerData().getBirthDate());
		transactionLog.setServiceName("AccountIncomingRemittance");
		transactionLog.setDateLogged(new Date());
		transactionDao.save(transactionLog);
	}

}