package com.mfs.client.gimac.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.mfs.client.gimac.dao.LogAuditDao;
import com.mfs.client.gimac.models.GimacAudit;
import com.mfs.client.gimac.service.BaseAuditService;

public class BaseAuditServiceImpl implements BaseAuditService {

	@Autowired
	LogAuditDao auditDao;

	public void logAuditDetails(String issuerTrxRef, String intent, String senderMobile, String accountNumber,
			String toMember, double amount, String currency, String senderFirstName, String senderSecondName,
			String senderIdNumber, String receiverFirstName, String receiverSecondName, String receiverIdNumber,
			String state, String receiverPhone, String voucherCode, Date dateLogged, String serviceName, String status,
			String message) {

		GimacAudit audit = new GimacAudit();
		audit.setIssuerTrxRef(issuerTrxRef);
		audit.setIntent(intent);
		audit.setSenderMobile(senderMobile);
		audit.setAccountNumber(accountNumber);
		audit.setToMember(toMember);
		audit.setAmount(amount);
		audit.setCurrency(currency);
		audit.setSenderFirstName(senderFirstName);
		audit.setSenderSecondName(senderSecondName);
		audit.setSenderIdNumber(senderIdNumber);
		audit.setReceiverFirstName(receiverFirstName);
		audit.setReceiverSecondName(receiverSecondName);
		audit.setReceiverIdNumber(receiverIdNumber);
		audit.setState(state);
		audit.setStatus(status);
		audit.setReceiverPhone(receiverPhone);
		audit.setVoucherCode(voucherCode);
		audit.setDateLogged(dateLogged);
		audit.setServiceName(serviceName);
		audit.setStatus(status);
		audit.setMessage(message);

		auditDao.logAuditDetails(audit);

	}

	public void logAuditCancellation(String issuerTrxRef, String intent, String state, String voucherCode,
			String serviceName, Date dateLogged, String status, String message) {

		GimacAudit audit = new GimacAudit();
		audit.setIssuerTrxRef(issuerTrxRef);
		audit.setIntent(intent);
		audit.setVoucherCode(voucherCode);
		audit.setState(state);
		audit.setDateLogged(dateLogged);
		audit.setServiceName(serviceName);
		audit.setStatus(status);
		audit.setMessage(message);
		auditDao.logAuditDetails(audit);

	}

	public void walletLogAuditDetails(String issuerTrxRef, String intent, String senderMobile, String walletdestination,
			String toMember, double amount, String currency, String senderFirstName, String senderSecondName,
			String senderIdNumber, String receiverFirstName, String receiverSecondName, String receiverIdNumber,
			String state, String receiverPhone, String voucherCode, Date dateLogged, String serviceName, String status,
			String message) {

		GimacAudit audit = new GimacAudit();
		audit.setIssuerTrxRef(issuerTrxRef);
		audit.setIntent(intent);
		audit.setSenderMobile(senderMobile);
		//audit.setWalletDestination(walletdestination);
		audit.setToMember(toMember);
		audit.setAmount(amount);
		audit.setCurrency(currency);
		audit.setSenderFirstName(senderFirstName);
		audit.setSenderSecondName(senderSecondName);
		audit.setSenderIdNumber(senderIdNumber);
		audit.setReceiverFirstName(receiverFirstName);
		audit.setReceiverSecondName(receiverSecondName);
		audit.setReceiverIdNumber(receiverIdNumber);
		audit.setState(state);
		audit.setStatus(status);
		audit.setReceiverPhone(receiverPhone);
		audit.setVoucherCode(voucherCode);
		audit.setDateLogged(dateLogged);
		audit.setServiceName(serviceName);
		audit.setStatus(status);
		audit.setMessage(message);

		auditDao.logAuditDetails(audit);

	}

	public void walletCancellationlogAudit(String issuerTrxRef, String intent, String state, String voucherCode,
			String serviceName, Date dateLogged, String status, String message) {

		GimacAudit audit = new GimacAudit();
		audit.setIssuerTrxRef(issuerTrxRef);
		audit.setIntent(intent);
		audit.setVoucherCode(voucherCode);
		audit.setState(state);
		audit.setDateLogged(dateLogged);
		audit.setServiceName(serviceName);
		audit.setStatus(status);
		audit.setMessage(message);
		auditDao.logAuditDetails(audit);

	}
   public void paymentInquirylogAudit(String issuerTrxRef,String serviceName, Date dateLogged,String voucherCode,String status,String message) {
	   GimacAudit audit=new GimacAudit();
	   audit.setIssuerTrxRef(issuerTrxRef);
	   audit.setServiceName(serviceName);
	   audit.setDateLogged(dateLogged);
	   audit.setVoucherCode(voucherCode);
	   audit.setStatus(status);
	   audit.setMessage(message);
	   auditDao.logAuditDetails(audit);
	   
	   
   }
}
