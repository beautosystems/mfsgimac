package com.mfs.client.gimac.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gimac_event_log")
public class GimacEventLog {

	@Id
	@GeneratedValue
	@Column(name = "event_id")
	private int eventId;
	
	@Column(name = "service_name")
	private String serviceName;
	
	@Column(name = "mfs_id")
	private String mfsId;
	
	@Column(name = "request" , columnDefinition = "text", length = 65535)
	private String request;
	
	@Column(name = "response" , columnDefinition = "text", length = 65535)
	private String response;
	
	@Column(name = "date_logged")
	private Date dateLogged;

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "GimacEventLog [eventId=" + eventId + ", serviceName=" + serviceName + ", mfsId=" + mfsId + ", request="
				+ request + ", response=" + response + ", dateLogged=" + dateLogged + "]";
	}
	
}