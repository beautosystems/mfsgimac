package com.mfs.client.gimac.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.gimac.dao.SystemConfigDao;
import com.mfs.client.gimac.dao.TransactionDao;
import com.mfs.client.gimac.dto.RemittanceCancellationRequestDto;
import com.mfs.client.gimac.dto.RemittanceCancellationResponseDto;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacAuthentication;
import com.mfs.client.gimac.models.GimacEventLog;
import com.mfs.client.gimac.models.GimacTransactionError;
import com.mfs.client.gimac.models.GimacTransactionLog;
import com.mfs.client.gimac.service.IncomingRemittanceCancellationService;
import com.mfs.client.gimac.util.CallServices;
import com.mfs.client.gimac.util.CommonConstant;
import com.mfs.client.gimac.util.ExpiredToken;
import com.mfs.client.gimac.util.HttpsConnectionRequest;
import com.mfs.client.gimac.util.HttpsConnectionResponse;
import com.mfs.client.gimac.util.HttpsConnectorImpl;
import com.mfs.client.gimac.util.JSONToObjectConversion;
import com.mfs.client.gimac.util.MFSGimacCodes;

/*
* @author Arnav Gharote
* IncomingRemittanceCancellationService.java
* 26-05-2020
*
* purpose: this class is used for IncomingRemittanceCancellationService
*
* Methods
* 1. incomingRemittanceCancellation
* 2. log request in audit
* 2. log response
* 3. log error request and response
*
*/
@Service("IncomingRemittanceCancelationService")
public class IncomingRemittanceCancellationServiceImpl extends BaseAuditServiceImpl
		implements IncomingRemittanceCancellationService {

	private static final Logger LOGGER = Logger.getLogger(IncomingRemittanceCancellationServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDao systemConfigDao;

	public RemittanceCancellationResponseDto incomingRemittanceCancellation(RemittanceCancellationRequestDto request) {

		Map<String, String> headerMap = new HashMap<String, String>();
		Gson gson = new Gson();
		RemittanceCancellationResponseDto response = null;
		String serviceResponse = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();

		try {

			// Log request in audit table
			logAuditCancellation(request.getIssuerTrxRef(), request.getIntent(), request.getState(),
					request.getVoucherCode(), CommonConstant.INCOMING_REMITTANCE_CANCELLATION_SERVICE, new Date(),
					CommonConstant.INITIATE_REQUEST, "null");

			// get data of issureTrxRef from db
			GimacTransactionLog transactionLog = transactionDao.getTransactionByIssuerTrxRef(request.getIssuerTrxRef());
			if (transactionLog != null) {

				// get last generated Token ID
				GimacAuthentication accessToken = transactionDao.getLastRecordByAccessTokenId();
				if (accessToken != null) {

					// check for expire token
					Date lastDBDate = accessToken.getDateLogged();
					boolean flag = ExpiredToken.isTokenExpire(lastDBDate, new Date());
					if (flag) {

						// get Config Details
						Map<String, String> configMap = systemConfigDao.getConfigDetailsMap();

						// check for status
						if (transactionLog.getState().equalsIgnoreCase(configMap.get(CommonConstant.STATUS))) {

							// set service URL
							connectionRequest.setHttpmethodName("POST");
							connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
							connectionRequest.setServiceUrl(
									configMap.get(CommonConstant.BASE_URL) + configMap.get(CommonConstant.PAYMENT) + "/"
											+ configMap.get(CommonConstant.UPDATE));
							// set Headers
							headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
							connectionRequest.setHeaders(headerMap);

							String req = gson.toJson(request);

							// eventLog
							eventRequestLog(req, request.getIssuerTrxRef());

							logAuditCancellation(request.getIssuerTrxRef(), request.getIntent(), request.getState(),
									request.getVoucherCode(), CommonConstant.INCOMING_REMITTANCE_CANCELLATION_SERVICE,
									new Date(), CommonConstant.INITIATE_REQUEST_TO_PARTNER, "null");

							LOGGER.info(
									"IncomingRemittanceCancellationServiceImpl in incomingRemittanceCancellation function request "
											+ req);

							// getting gimac response
							// serviceResponse =CallServices.getResponseFromService(connectionRequest,req);

							LOGGER.info(
									" IncomingRemittanceCancellationServiceImpl in IncomingRemittanceCancellationService function response "
											+ serviceResponse);

							serviceResponse = "{\"issuerTrxRef\":\"" + request.getIssuerTrxRef() + "\","
									+ "\"voucherCode\":\"" + request.getVoucherCode() + "\","
									+ "\"state\":\"CANCELLED\",\"updateTime\":\"1490109044\"}";

							// String serviceResponse = "{\"error\":\"invalid_grant\","
							// + "\"error_description\":\"Bad credentials\"}";

							eventResponseLog(serviceResponse);

							response = (RemittanceCancellationResponseDto) JSONToObjectConversion
									.getObjectFromJson(serviceResponse, RemittanceCancellationResponseDto.class);

							// Check for success and fail
							if (response != null) {

								logResponse(request, response);

							} else {
								// Null response
								response = new RemittanceCancellationResponseDto();
								response.setCode(MFSGimacCodes.ER201.getCode());
								response.setMessage(MFSGimacCodes.ER201.getMessage());
							}
						} else {
							// Transaction is cancelled
							response = new RemittanceCancellationResponseDto();
							response.setCode(MFSGimacCodes.ER217.getCode());
							response.setMessage(MFSGimacCodes.ER217.getMessage());
						}
					} else {
						response = new RemittanceCancellationResponseDto();
						response.setCode(MFSGimacCodes.TOKEN_EXPIRED.getCode());
						response.setMessage(MFSGimacCodes.TOKEN_EXPIRED.getMessage());
					}
				} else {

					// Null Access Token
					response = new RemittanceCancellationResponseDto();
					response.setCode(MFSGimacCodes.ER216.getCode());
					response.setMessage(MFSGimacCodes.ER216.getMessage());
				}
			} else {
				// No record found in db
				response = new RemittanceCancellationResponseDto();
				response.setCode(MFSGimacCodes.ER218.getCode());
				response.setMessage(MFSGimacCodes.ER218.getMessage());

			}
			logAuditCancellation(response.getIssuerTrxRef(), request.getIntent(), response.getState(),
					response.getVoucherCode(), CommonConstant.INCOMING_REMITTANCE_CANCELLATION_SERVICE, new Date(),
					CommonConstant.RESPONSE_FROM_PARTNER, response.getMessage());

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in IncomingRemittanceCancellationServiceImpl " + de);
			response = new RemittanceCancellationResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in IncomingRemittanceCancellationServiceImpl" + e);
			response = new RemittanceCancellationResponseDto();
			response.setCode(MFSGimacCodes.ER207.getCode());
			response.setMessage(MFSGimacCodes.ER207.getMessage());
		}
		return response;
	}

	private void eventResponseLog(String response) throws DaoException {

		GimacEventLog transactionEventLog = transactionDao.getByLastRecord();

		if (transactionEventLog != null) {
			transactionEventLog.setResponse(response);
			transactionEventLog.setDateLogged(new Date());
			transactionDao.update(transactionEventLog);

		}
	}

	private void eventRequestLog(String request, String issuerTrxRef) throws DaoException {

		GimacEventLog eventLog = new GimacEventLog();

		eventLog.setMfsId(issuerTrxRef);
		eventLog.setRequest(request);
		eventLog.setServiceName(CommonConstant.INCOMING_REMITTANCE_CANCELLATION_SERVICE);
		eventLog.setDateLogged(new Date());
		transactionDao.save(eventLog);
	}

	// update request and response
	private void logResponse(RemittanceCancellationRequestDto request, RemittanceCancellationResponseDto response)
			throws DaoException {
		GimacTransactionLog transactionLog = transactionDao.getTransactionByIssuerTrxRef(request.getIssuerTrxRef());
		if (transactionLog != null) {
			transactionLog.setIssuerTrxRef(request.getIssuerTrxRef());
			transactionLog.setIntent(request.getIntent());
			transactionLog.setUpdateTime(response.getUpdateTime());
			transactionLog.setVoucherCode(request.getVoucherCode());
			transactionLog.setState(response.getState());
			transactionLog.setServiceName("IncomingRemittanceCancelltion");
			transactionLog.setDateLogged(new Date());
			transactionDao.update(transactionLog);
		}

	}
}
