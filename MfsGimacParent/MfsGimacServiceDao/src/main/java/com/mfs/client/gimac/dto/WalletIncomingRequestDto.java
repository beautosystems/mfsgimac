package com.mfs.client.gimac.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class WalletIncomingRequestDto {
	private String issuerTrxRef;
	private String intent;
	private String createTime;
	private String senderMobile;
	private String walletDestination;
	private String toMember;
	private String description;
	private double amount;
	private String currency;
	private SenderCustomerDataDto senderCustomerData;
	private ReceiverCustomerDataDto   receiverCustomerData;
	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}
	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getSenderMobile() {
		return senderMobile;
	}
	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}
	public String getWalletDestination() {
		return walletDestination;
	}
	public void setWalletDestination(String walletDestination) {
		this.walletDestination = walletDestination;
	}
	public String getToMember() {
		return toMember;
	}
	public void setToMember(String toMember) {
		this.toMember = toMember;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public SenderCustomerDataDto getSenderCustomerData() {
		return senderCustomerData;
	}
	public void setSenderCustomerData(SenderCustomerDataDto senderCustomerData) {
		this.senderCustomerData = senderCustomerData;
	}
	public ReceiverCustomerDataDto getReceiverCustomerData() {
		return receiverCustomerData;
	}
	public void setReceiverCustomerData(ReceiverCustomerDataDto receiverCustomerData) {
		this.receiverCustomerData = receiverCustomerData;
	}
	@Override
	public String toString() {
		return "WalletIncomingRequestDto [issuerTrxRef=" + issuerTrxRef + ", intent=" + intent + ", createTime="
				+ createTime + ", senderMobile=" + senderMobile + ", walletDestination=" + walletDestination
				+ ", toMember=" + toMember + ", description=" + description + ", amount=" + amount + ", currency="
				+ currency + ", senderCustomerData=" + senderCustomerData + ", receiverCustomerData="
				+ receiverCustomerData + "]";
	}
	
	
}
