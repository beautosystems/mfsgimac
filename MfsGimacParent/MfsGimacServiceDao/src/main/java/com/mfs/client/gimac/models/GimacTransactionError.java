package com.mfs.client.gimac.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gimac_transaction_error")
public class GimacTransactionError {

	@Id
	@GeneratedValue
	@Column(name = "transaction_error_id")
	private int transactionErrorId;

	@Column(name = "issuer_trx_ref")
	private String issuerTrxRef;

	@Column(name = "intent")
	private String intent;

	@Column(name = "sender_mobile")
	private String senderMobile;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "to_member")
	private String toMember;

	@Column(name = "amount")
	private double amount;

	@Column(name = "currency")
	private String currency;

	@Column(name = "sender_first_name")
	private String senderFirstName;

	@Column(name = "sender_second_name")
	private String senderSecondName;

	@Column(name = "sender_id_number")
	private String senderIdNumber;

	@Column(name = "receiver_first_name")
	private String receiverFirstName;

	@Column(name = "receiver_second_name")
	private String receiverSecondName;

	@Column(name = "receiver_id_number")
	private String receiverIdNumber;

	@Column(name = "state")
	private String state;

	@Column(name = "voucher_code")
	private String voucherCode;

	@Column(name = "acquirer_trx_ref")
	private String acquirerTrxRef;

	@Column(name = "date_logged")
	private Date dateLogged;

	@Column(name = "wallet_destination")
	private String walletDestination;

	@Column(name = "from_member")
	private String fromMember;

	@Column(name = "service_name")
	private String serviceName;

	@Column(name = "error")
	private String error;

	@Column(name = "error_description")
	private String errorDescription;

	public int getTransactionErrorId() {
		return transactionErrorId;
	}

	public void setTransactionErrorId(int transactionErrorId) {
		this.transactionErrorId = transactionErrorId;
	}

	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}

	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}

	public String getIntent() {
		return intent;
	}

	public void setIntent(String intent) {
		this.intent = intent;
	}

	public String getSenderMobile() {
		return senderMobile;
	}

	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getToMember() {
		return toMember;
	}

	public void setToMember(String toMember) {
		this.toMember = toMember;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	public String getSenderSecondName() {
		return senderSecondName;
	}

	public void setSenderSecondName(String senderSecondName) {
		this.senderSecondName = senderSecondName;
	}

	public String getSenderIdNumber() {
		return senderIdNumber;
	}

	public void setSenderIdNumber(String senderIdNumber) {
		this.senderIdNumber = senderIdNumber;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverSecondName() {
		return receiverSecondName;
	}

	public void setReceiverSecondName(String receiverSecondName) {
		this.receiverSecondName = receiverSecondName;
	}

	public String getReceiverIdNumber() {
		return receiverIdNumber;
	}

	public void setReceiverIdNumber(String receiverIdNumber) {
		this.receiverIdNumber = receiverIdNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getAcquirerTrxRef() {
		return acquirerTrxRef;
	}

	public void setAcquirerTrxRef(String acquirerTrxRef) {
		this.acquirerTrxRef = acquirerTrxRef;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getWalletDestination() {
		return walletDestination;
	}

	public void setWalletDestination(String walletDestination) {
		this.walletDestination = walletDestination;
	}

	public String getFromMember() {
		return fromMember;
	}

	public void setFromMember(String fromMember) {
		this.fromMember = fromMember;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	@Override
	public String toString() {
		return "TransactionErrorModel [transactionErrorId=" + transactionErrorId + ", issuerTrxRef=" + issuerTrxRef
				+ ", intent=" + intent + ", senderMobile=" + senderMobile + ", accountNumber=" + accountNumber
				+ ", toMember=" + toMember + ", amount=" + amount + ", currency=" + currency + ", senderFirstName="
				+ senderFirstName + ", senderSecondName=" + senderSecondName + ", senderIdNumber=" + senderIdNumber
				+ ", receiverFirstName=" + receiverFirstName + ", receiverSecondName=" + receiverSecondName
				+ ", receiverIdNumber=" + receiverIdNumber + ", state=" + state + ", voucherCode=" + voucherCode
				+ ", acquirerTrxRef=" + acquirerTrxRef + ", dateLogged=" + dateLogged + ", walletDestination="
				+ walletDestination + ", fromMember=" + fromMember + ", serviceName=" + serviceName + ", error=" + error
				+ ", errorDescription=" + errorDescription + "]";
	}

}