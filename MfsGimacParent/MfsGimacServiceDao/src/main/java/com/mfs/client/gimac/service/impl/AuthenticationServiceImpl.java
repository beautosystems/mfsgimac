package com.mfs.client.gimac.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.gimac.dao.SystemConfigDao;
import com.mfs.client.gimac.dao.TransactionDao;
import com.mfs.client.gimac.dto.AccountIncomingResponseDto;
import com.mfs.client.gimac.dto.AuthenticationRequestDto;
import com.mfs.client.gimac.dto.AuthenticationResponseDto;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacAuthentication;
import com.mfs.client.gimac.service.AuthenticationService;
import com.mfs.client.gimac.util.CallServices;
import com.mfs.client.gimac.util.CommonConstant;
import com.mfs.client.gimac.util.HttpsConnectionRequest;
import com.mfs.client.gimac.util.HttpsConnectionResponse;
import com.mfs.client.gimac.util.JSONToObjectConversion;
import com.mfs.client.gimac.util.MFSGimacCodes;

/**
 * @ author Pallavi AuthenticationServiceImpl.java 22-May-2020
 * 
 * purpose : this class is used for Authentication
 * 
 * Methods 1. authenticationService
 * 
 * Change_history
 * 
 */
@Service("AuthenticationService")
public class AuthenticationServiceImpl implements AuthenticationService {

	private static final Logger LOGGER = Logger.getLogger(AuthenticationServiceImpl.class);

	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	// This Function is use to call Auth endpoint and get response
	public AuthenticationResponseDto authenticationService() {
		Map<String, String> systemConfigDetails = null;
		Gson gson = new Gson();
		String authResponse = null;

		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		AuthenticationResponseDto authorizationResponse = new AuthenticationResponseDto();
		HttpsConnectionResponse httpsConResult = null;

		try {
			// get Config Details from DB
			systemConfigDetails = systemConfigDao.getConfigDetailsMap();

			AuthenticationRequestDto authorizationRequest = new AuthenticationRequestDto();
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			authorizationRequest.setGrant_type(systemConfigDetails.get(CommonConstant.GRANT_TYPE));
			authorizationRequest.setClient_id(systemConfigDetails.get(CommonConstant.CLIENT_ID));
			authorizationRequest.setClient_secret(systemConfigDetails.get(CommonConstant.CLIENT_SECRET));
			authorizationRequest.setScope(CommonConstant.SCOPE);
			authorizationRequest.setUsername(systemConfigDetails.get(CommonConstant.USERNAME));
			authorizationRequest.setPassword(systemConfigDetails.get(CommonConstant.PASSWORD));

			String req = gson.toJson(authorizationRequest);

			LOGGER.info("AuthenticationServiceImpl in authenticationService function request " + req);

			// getting gimac response
			authResponse = CallServices.getResponseFromService1(systemConfigDetails.get(CommonConstant.AUTH_URL), req);

			LOGGER.info("AuthorizationServiceImpl in authorizationService function response : " + authResponse);

			// Dummy Response
			authResponse = "{\n" + "   \"access_token\":\"42c93229-59b2-49fd-9366-69f5e7177712\",\n"
					+ "   \"token_type\":\"bearer\",\n"
					+ "   \"refresh_token\":\"61ecd5fc-53bb-447e-bcb4-8d8873e59876\",\n" + "   \"expires_in\":14400,\n"
					+ "   \"scope\":\"read\"\n" + "}";
			authorizationResponse = (AuthenticationResponseDto) JSONToObjectConversion.getObjectFromJson(authResponse,
					AuthenticationResponseDto.class);

			// check success and fail response
			if (authorizationResponse == null) {
				authorizationResponse = new AuthenticationResponseDto();
				authorizationResponse.setCode(MFSGimacCodes.ER201.getCode());
				authorizationResponse.setMessage(MFSGimacCodes.ER201.getMessage());

			} else {
				// log auth response
				logResponse(authorizationResponse);
			}

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in AuthorizationServiceImpl" + de);
			authorizationResponse.setCode(de.getStatus().getStatusCode());
			authorizationResponse.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in AuthorizationServiceImpl" + e);
			authorizationResponse.setCode(MFSGimacCodes.ER207.getCode());
			authorizationResponse.setMessage(MFSGimacCodes.ER207.getMessage());
		}

		return authorizationResponse;
	}

	private void logResponse(AuthenticationResponseDto response) throws DaoException {
		GimacAuthentication auth = new GimacAuthentication();
		auth.setAccessToken(response.getAccess_token());
		auth.setTokenType(response.getToken_type());
		auth.setRefreshToken(response.getRefresh_token());
		auth.setExpiresIn(Integer.toString(response.getExpires_in()));
		auth.setScope(response.getScope());
		auth.setDateLogged(new Date());
		transactionDao.save(auth);
	}

}