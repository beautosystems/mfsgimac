package com.mfs.client.gimac.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.gimac.dao.SystemConfigDao;
import com.mfs.client.gimac.dto.ResponseStatus;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacSystemConfig;

@EnableTransactionManagement
@Repository("SystemConfigDao")
@Component
public class SystemConfigDaoImpl implements SystemConfigDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(SystemConfigDaoImpl.class);

	@Transactional
	public Map<String, String> getConfigDetailsMap() throws DaoException {

		LOGGER.info("Inside  getConfigDetailsMap");
		Map<String, String> systemConfigMap = new HashMap<String, String>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "From GimacSystemConfig";
		Query query = session.createQuery(hql);

		
			List<GimacSystemConfig> systemConfig = query.list();

			if (systemConfig != null && !systemConfig.isEmpty()) {
				systemConfigMap = new HashMap<String, String>();
				for (GimacSystemConfig systemConfiguration : systemConfig) {
					systemConfigMap.put(systemConfiguration.getConfigKey(), systemConfiguration.getConfigValue());
				}
			}
			if (systemConfigMap != null && !systemConfigMap.isEmpty()) {
				LOGGER.debug("getConfigDetailsMap response size -> " + systemConfigMap.size());
			}
		return systemConfigMap;
	}
}
