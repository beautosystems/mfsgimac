package com.mfs.client.gimac.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gimac_authentication")
public class GimacAuthentication {

	@Id
	@GeneratedValue
	@Column(name = "authentication_id")
	private int authenticationId;

	@Column(name = "refresh_token")
	private String refreshToken;

	@Column(name = "access_token")
	private String accessToken;

	@Column(name = "scope")
	private String scope;

	@Column(name = "expires_in")
	private String expiresIn;

	@Column(name = "token_type")
	private String tokenType;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getAuthenticationId() {
		return authenticationId;
	}

	public void setAuthenticationId(int authenticationId) {
		this.authenticationId = authenticationId;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "GimacAuthentication [authenticationId=" + authenticationId + ", refreshToken=" + refreshToken
				+ ", accessToken=" + accessToken + ", scope=" + scope + ", expiresIn=" + expiresIn + ", tokenType="
				+ tokenType + ", dateLogged=" + dateLogged + "]";
	}

}