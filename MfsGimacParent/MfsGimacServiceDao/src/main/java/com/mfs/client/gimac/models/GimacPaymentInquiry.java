package com.mfs.client.gimac.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gimac_payment_inquiry")
public class GimacPaymentInquiry {

	@Id
	@GeneratedValue
	@Column(name = "payment_id")
	private int paymentId;

	@Column(name = "as_issuer")
	private String asIssuer;

	@Column(name = "as_acquirer")
	private String asAcquirer;

	@Column(name = "first_element")
	private String firstElement;

	@Column(name = "page_size")
	private String pageSize;

	@Column(name = "issuer_trx_ref")
	private String issuerTrxRef;

	@Column(name = "acquirer_trx_ref")
	private String acquirerTrxRef;

	@Column(name = "voucher_code")
	private String voucherCode;

	@Column(name = "status")
	private String status;

	@Column(name = "intents")
	private String intents;

	@Column(name = "from_member")
	private String fromMember;

	@Column(name = "to_member")
	private String toMember;

	@Column(name = "from_date")
	private String fromDate;

	@Column(name = "to_date")
	private String toDate;

	@Column(name = "intent")
	private String intent;

	@Column(name = "amount")
	private double amount;

	@Column(name = "currency")
	private String currency;

	@Column(name = "state")
	private String state;

	@Column(name = "description")
	private String description;

	@Column(name = "create_time")
	private String createTime;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "sender_mobile")
	private String senderMobile;

	@Column(name = "validity_duration")
	private String validityDuration;

	@Column(name = "wallet_destination")
	private String walletDestination;

	@Column(name = "wallet_source")
	private String walletSource;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	public String getAsIssuer() {
		return asIssuer;
	}

	public void setAsIssuer(String asIssuer) {
		this.asIssuer = asIssuer;
	}

	public String getAsAcquirer() {
		return asAcquirer;
	}

	public void setAsAcquirer(String asAcquirer) {
		this.asAcquirer = asAcquirer;
	}

	public String getFirstElement() {
		return firstElement;
	}

	public void setFirstElement(String firstElement) {
		this.firstElement = firstElement;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}

	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}

	public String getAcquirerTrxRef() {
		return acquirerTrxRef;
	}

	public void setAcquirerTrxRef(String acquirerTrxRef) {
		this.acquirerTrxRef = acquirerTrxRef;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIntents() {
		return intents;
	}

	public void setIntents(String intents) {
		this.intents = intents;
	}

	public String getFromMember() {
		return fromMember;
	}

	public void setFromMember(String fromMember) {
		this.fromMember = fromMember;
	}

	public String getToMember() {
		return toMember;
	}

	public void setToMember(String toMember) {
		this.toMember = toMember;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getIntent() {
		return intent;
	}

	public void setIntent(String intent) {
		this.intent = intent;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSenderMobile() {
		return senderMobile;
	}

	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}

	public String getValidityDuration() {
		return validityDuration;
	}

	public void setValidityDuration(String validityDuration) {
		this.validityDuration = validityDuration;
	}

	public String getWalletDestination() {
		return walletDestination;
	}

	public void setWalletDestination(String walletDestination) {
		this.walletDestination = walletDestination;
	}

	public String getWalletSource() {
		return walletSource;
	}

	public void setWalletSource(String walletSource) {
		this.walletSource = walletSource;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "GimacPaymentInquiry [paymentId=" + paymentId + ", asIssuer=" + asIssuer + ", asAcquirer=" + asAcquirer
				+ ", firstElement=" + firstElement + ", pageSize=" + pageSize + ", issuerTrxRef=" + issuerTrxRef
				+ ", acquirerTrxRef=" + acquirerTrxRef + ", voucherCode=" + voucherCode + ", status=" + status
				+ ", intents=" + intents + ", fromMember=" + fromMember + ", toMember=" + toMember + ", fromDate="
				+ fromDate + ", toDate=" + toDate + ", intent=" + intent + ", amount=" + amount + ", currency="
				+ currency + ", state=" + state + ", description=" + description + ", createTime=" + createTime
				+ ", accountNumber=" + accountNumber + ", senderMobile=" + senderMobile + ", validityDuration="
				+ validityDuration + ", walletDestination=" + walletDestination + ", walletSource=" + walletSource
				+ ", dateLogged=" + dateLogged + "]";
	}

}