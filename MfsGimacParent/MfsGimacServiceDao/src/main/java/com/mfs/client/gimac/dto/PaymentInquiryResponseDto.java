package com.mfs.client.gimac.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class PaymentInquiryResponseDto {

	private String intent;
	private String voucherCode;
	private double amount;
	private String currency;
	private String state;
	private String description;
	private String issuerTrxRef;
	private String toMember;
	private String fromMember;
	private String createTime;
	private String acquirerTrxRef;
	private String accountNumber;
	private String senderMobile;
	private String validityDuration;
	private String walletDestination;
	private String walletSource;
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}
	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}
	public String getToMember() {
		return toMember;
	}
	public void setToMember(String toMember) {
		this.toMember = toMember;
	}
	public String getFromMember() {
		return fromMember;
	}
	public void setFromMember(String fromMember) {
		this.fromMember = fromMember;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getAcquirerTrxRef() {
		return acquirerTrxRef;
	}
	public void setAcquirerTrxRef(String acquirerTrxRef) {
		this.acquirerTrxRef = acquirerTrxRef;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getSenderMobile() {
		return senderMobile;
	}
	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}
	public String getValidityDuration() {
		return validityDuration;
	}
	public void setValidityDuration(String validityDuration) {
		this.validityDuration = validityDuration;
	}
	public String getWalletDestination() {
		return walletDestination;
	}
	public void setWalletDestination(String walletDestination) {
		this.walletDestination = walletDestination;
	}
	public String getWalletSource() {
		return walletSource;
	}
	public void setWalletSource(String walletSource) {
		this.walletSource = walletSource;
	}
	@Override
	public String toString() {
		return "PaymentInquiryResponseDto [intent=" + intent + ", voucherCode=" + voucherCode + ", amount=" + amount
				+ ", currency=" + currency + ", state=" + state + ", description=" + description + ", issuerTrxRef="
				+ issuerTrxRef + ", toMember=" + toMember + ", fromMember=" + fromMember + ", createTime=" + createTime
				+ ", acquirerTrxRef=" + acquirerTrxRef + ", accountNumber=" + accountNumber + ", senderMobile="
				+ senderMobile + ", validityDuration=" + validityDuration + ", walletDestination=" + walletDestination
				+ ", walletSource=" + walletSource + "]";
	}
	

}
