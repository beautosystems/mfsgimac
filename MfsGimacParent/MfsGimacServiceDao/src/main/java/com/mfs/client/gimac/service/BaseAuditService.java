package com.mfs.client.gimac.service;

import java.util.Date;

public interface BaseAuditService {
	public void logAuditDetails(String issuerTrxRef, String intent, String senderMobile, String accountNumber,
			String toMember, double amount, String currency, String senderFirstName, String senderSecondName,
			String senderIdNumber, String receiverFirstName, String receiverSecondName, String receiverIdNumber,
			String state, String receiverPhone, String voucherCode,  Date dateLogged,
			String serviceName, String status, String message);
}
