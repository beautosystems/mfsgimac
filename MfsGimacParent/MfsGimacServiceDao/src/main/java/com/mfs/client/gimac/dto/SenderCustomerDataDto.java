package com.mfs.client.gimac.dto;

public class SenderCustomerDataDto {

	private String firstName;
	private String secondName;
	private String idType;
	private String idNumber;
	private String address;
	private String birthDate;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		return "SenderCustomerDataDto [firstName=" + firstName + ", secondName=" + secondName + ", idType=" + idType
				+ ", idNumber=" + idNumber + ", address=" + address + ", birthDate=" + birthDate + "]";
	}

}
