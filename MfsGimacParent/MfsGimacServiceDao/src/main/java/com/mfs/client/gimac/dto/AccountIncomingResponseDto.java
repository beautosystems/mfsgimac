package com.mfs.client.gimac.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class AccountIncomingResponseDto {
	
	private String issuerTrxRef;
	private String intent;
	private String createTime;
	private String senderMobile;
	private String accountNumber;
	private String toMember;
	private String description;
	private double amount;
	private String currency;
	private String state;
	private String voucherCode;
	private String acquirerTrxRef;
	
	private SenderCustomerDataDto senderCustomerData;
	private ReceiverCustomerDataDto receiverCustomerData;
	
	private String error;
	private String errorDescription;
	
	private String code;
	private String message;
	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}
	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getSenderMobile() {
		return senderMobile;
	}
	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getToMember() {
		return toMember;
	}
	public void setToMember(String toMember) {
		this.toMember = toMember;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	public String getAcquirerTrxRef() {
		return acquirerTrxRef;
	}
	public void setAcquirerTrxRef(String acquirerTrxRef) {
		this.acquirerTrxRef = acquirerTrxRef;
	}
	public SenderCustomerDataDto getSenderCustomerData() {
		return senderCustomerData;
	}
	public void setSenderCustomerData(SenderCustomerDataDto senderCustomerData) {
		this.senderCustomerData = senderCustomerData;
	}
	public ReceiverCustomerDataDto getReceiverCustomerData() {
		return receiverCustomerData;
	}
	public void setReceiverCustomerData(ReceiverCustomerDataDto receiverCustomerData) {
		this.receiverCustomerData = receiverCustomerData;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	@Override
	public String toString() {
		return "AccountIncomingResponseDto [issuerTrxRef=" + issuerTrxRef + ", intent=" + intent + ", createTime="
				+ createTime + ", senderMobile=" + senderMobile + ", accountNumber=" + accountNumber + ", toMember="
				+ toMember + ", description=" + description + ", amount=" + amount + ", currency=" + currency
				+ ", state=" + state + ", voucherCode=" + voucherCode + ", acquirerTrxRef=" + acquirerTrxRef
				+ ", senderCustomerData=" + senderCustomerData + ", receiverCustomerData=" + receiverCustomerData
				+ ", error=" + error + ", errorDescription=" + errorDescription + ", code=" + code + ", message="
				+ message + "]";
	}
	
	
	
	
}
