package com.mfs.client.gimac.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.gimac.dao.TransactionDao;
import com.mfs.client.gimac.dto.ResponseStatus;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacAuthentication;
import com.mfs.client.gimac.models.GimacEventLog;
import com.mfs.client.gimac.models.GimacPaymentInquiry;
import com.mfs.client.gimac.models.GimacTransactionLog;
import com.mfs.client.gimac.util.MFSGimacCodes;

@EnableTransactionManagement
@Repository("TransactionDao")
public class TransactionDaoImpl extends BaseDaoImpl implements TransactionDao {

	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class);

	@Transactional
	public GimacTransactionLog getTransactionByIssuerTrxRef(String issuerTrxRef) throws DaoException {
		LOGGER.debug("Inside getTansactionLogByexttrId of TransactionDaoImpl request:" + issuerTrxRef);
		GimacTransactionLog transLogModel = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			transLogModel = (GimacTransactionLog) session
					.createQuery("From GimacTransactionLog where issuerTrxRef=:issuerTrxRef")
					.setParameter("issuerTrxRef", issuerTrxRef).uniqueResult();
		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(MFSGimacCodes.ER202.getMessage());
			status.setStatusCode(MFSGimacCodes.ER202.getCode());
		}

		return transLogModel;
	}

	@Transactional
	public GimacAuthentication getLastRecordByAccessTokenId() {
		GimacAuthentication authorizationModel = null;

		Session session = sessionFactory.getCurrentSession();
		try {
			authorizationModel = (GimacAuthentication) session
					.createQuery("From GimacAuthentication order by authentication_id  DESC").list().get(0);

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(MFSGimacCodes.ER206.getMessage());
			status.setStatusCode(MFSGimacCodes.ER206.getCode());

		}
		return authorizationModel;
	}

	@Transactional
	public GimacPaymentInquiry getPaymentInquiryByIssuerTrxRef(String issuerTrxRef) throws DaoException {
		LOGGER.debug("Inside  getPaymentInquiryByIssuerTrxRef of TransactionDaoImpl request:" + issuerTrxRef);
		GimacPaymentInquiry gimacPaymentInquiry = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			gimacPaymentInquiry = (GimacPaymentInquiry) session
					.createQuery("From GimacPaymentInquiry where issuerTrxRef='" + issuerTrxRef + "'").uniqueResult();
		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(MFSGimacCodes.ER202.getMessage());
			status.setStatusCode(MFSGimacCodes.ER202.getCode());
		}

		return gimacPaymentInquiry;
	}

	@Transactional
	public GimacEventLog getByIssureTrxRefAndServiceName(String issureTrxRef, String serviceName) {
		GimacEventLog eventLog=null;
		Session session = sessionFactory.getCurrentSession();
		try {
			eventLog = (GimacEventLog) session
					.createQuery("From GimacEventLog where mfsId=:mfsId and serviceName=:serviceName")
					.setParameter("mfsId", issureTrxRef).setParameter("serviceName", serviceName).uniqueResult();

		} catch (Exception e) {
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(MFSGimacCodes.ER210.getMessage());
			status.setStatusCode(MFSGimacCodes.ER210.getCode());
		}
		return eventLog;
	}

		@Transactional
		public GimacEventLog getByLastRecord() {
			GimacEventLog eventLog = null;
			Session session = sessionFactory.getCurrentSession();

			try {
				eventLog = (GimacEventLog) session.createQuery("From GimacEventLog order by eventId DESC")
						.list().get(0);
			} catch (Exception e) {
				ResponseStatus status = new ResponseStatus();
				status.setStatusMessage(MFSGimacCodes.ER208.getMessage());
				status.setStatusCode(MFSGimacCodes.ER208.getCode());
			}
			return eventLog;
		
	}
			
	
}
