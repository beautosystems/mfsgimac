package com.mfs.client.gimac.service;

import com.mfs.client.gimac.dto.WalletIncomingCancellationRequestDto;
import com.mfs.client.gimac.dto.WalletIncomingCancellationResponseDto;

public interface WalletIncomingCancellationService {

	WalletIncomingCancellationResponseDto walletCancellation(WalletIncomingCancellationRequestDto request);

}
