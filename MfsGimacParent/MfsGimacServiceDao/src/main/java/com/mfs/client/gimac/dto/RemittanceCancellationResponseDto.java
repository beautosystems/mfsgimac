package com.mfs.client.gimac.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class RemittanceCancellationResponseDto {
	
	private String issuerTrxRef;
	private String updateTime;
	private String voucherCode;
	private String state;
	
	private String error;
	private String errorDescription;
	
	private String code;
	private String message;
	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}
	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "RemittanceCancelationResponseDto [issuerTrxRef=" + issuerTrxRef + ", updateTime=" + updateTime
				+ ", voucherCode=" + voucherCode + ", state=" + state + ", error=" + error + ", errorDescription="
				+ errorDescription + ", code=" + code + ", message=" + message + "]";
	}
	

}
