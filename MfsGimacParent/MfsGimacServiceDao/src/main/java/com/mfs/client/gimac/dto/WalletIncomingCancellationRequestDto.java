package com.mfs.client.gimac.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class WalletIncomingCancellationRequestDto {

	private String issuerTrxRef;
	private String intent;
	private String updateTime;
	private String voucherCode;
	private String state;
	
	
	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}
	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "WalletIncomingCancellationRequestDto [issuerTrxRef=" + issuerTrxRef + ", intent=" + intent
				+ ", updateTime=" + updateTime + ", voucherCode=" + voucherCode + ", state=" + state + "]";
	}
	
}
