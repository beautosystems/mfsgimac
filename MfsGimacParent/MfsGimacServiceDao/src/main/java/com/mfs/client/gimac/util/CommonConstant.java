package com.mfs.client.gimac.util;

import java.util.Date;

public class CommonConstant {

	public static final String BASE_URL = "base_url";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String APPLICATION_XML = "application/xml";
	public static final String TEXT_XML = "text/xml";

	public static final String PORT = "port";
	public static final String HTTP_METHOD = "httpmethod";
	public static final String ISHTTPS = "ishttps";

	public static final String USERNAME = "user_name";
	public static final String PASSWORD = "password";
	public static final String SCOPE = "read";
	public static final String GRANT_TYPE = "grant_type";
	public static final String CLIENT_ID = "client_id";
	public static final String CLIENT_SECRET = "client_secret";
	public static final String APPLICATION_URLENCODED = "application/x-www-form-urlencoded";
	public static final Object AUTH_URL = "auth_url";
	public static final int SECONDS = 14400;
	
	public static final String AUTHORIZATION="authorization";
	public static final String BEARER="Bearer";
	public static final String PAYMENT="payment";
	public static final String INQUIRY="inquiry";
	public static final String ASISSUER="asissuer";
	public static final String ASACQUIER="asacquirer";
	public static final String ISSUETRXREF="issuetrxref";
	public static final String VOUCHERCODE="vouchercode";
	public static final String STATUS="status";
	public static final String INTENTS="intents";
	
	public static final String INCOMING_REMMITANCE="incoming_remmitance";
	public static final String REMITTANCE_CANCELLATION = "remittance_cancellation";
	public static final String STATE = "NO REQUEST";
	public static final String MESSAGE = "No Request Message";
	public static final String PAYMENT_INQUIRY_SERVICE="payment inquiry service";
	public static final String SEND = "send";
	public static final String UPDATE = "update";
	public static final String SUCCESS_CODE = "200";
	public static final String ACCOUNT_INCOMING_REMITTANCE_SERVICE = "Account Incoming Remittance Service";
	public static final String INITIATE_REQUEST = "Initial request";
	public static final String INITIATE_REQUEST_TO_PARTNER = "Initiate request to partner";
	public static final String RESPONSE_FROM_PARTNER = "Response from partner";
	public static final String INCOMING_REMITTANCE_CANCELLATION_SERVICE = "Incoming Remittance Cancellation Service";
	public static final String WALLET_INCOMING_REMITTANCE_SERVICE = "Wallet Incoming Remittance Service";
	public static final String WALLET_INCOMING_CANCELLATION_SERVICE = "Wallet Incoming Cancellation Service";


	
		
}
