package com.mfs.client.gimac.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.gimac.dao.SystemConfigDao;
import com.mfs.client.gimac.dao.TransactionDao;
import com.mfs.client.gimac.dto.AccountIncomingResponseDto;
import com.mfs.client.gimac.dto.WalletIncomingRequestDto;
import com.mfs.client.gimac.dto.WalletIncomingResponseDto;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacAuthentication;
import com.mfs.client.gimac.models.GimacEventLog;
import com.mfs.client.gimac.models.GimacTransactionError;
import com.mfs.client.gimac.models.GimacTransactionLog;
import com.mfs.client.gimac.service.WalletIncomingRemittanceService;
import com.mfs.client.gimac.util.CallServices;
import com.mfs.client.gimac.util.CommonConstant;
import com.mfs.client.gimac.util.ExpiredToken;
import com.mfs.client.gimac.util.HttpsConnectionRequest;
import com.mfs.client.gimac.util.HttpsConnectionResponse;
import com.mfs.client.gimac.util.HttpsConnectorImpl;
import com.mfs.client.gimac.util.JSONToObjectConversion;
import com.mfs.client.gimac.util.MFSGimacCodes;

/*
* @author Priyanka Prakash Khade
* WalletIncomingRemittanceService.java
* 25-05-2020
*
* purpose: this class is used for WalletIncomingRemittanceService
*
* Methods
* 1.walletIncomingService
* 2.log request
* 3.log request in audit table
* 2.log response
* 3.log error request and response
*
*/
@Service("WalletIncomingRemittanceService")
public class WalletIncomingRemittanceServiceImpl extends BaseAuditServiceImpl
		implements WalletIncomingRemittanceService {

	private static final Logger LOGGER = Logger.getLogger(WalletIncomingRemittanceServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDao systemConfigDao;

	public WalletIncomingResponseDto walletIncomingService(WalletIncomingRequestDto request) {

		Map<String, String> headerMap = new HashMap<String, String>();
		Gson gson = new Gson();
		Map<String, String> configMap = null;
		String serviceResponse = null;
		WalletIncomingResponseDto response = new WalletIncomingResponseDto();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();

		try {

			// Log request in audit table
			walletLogAuditDetails(request.getIssuerTrxRef(), request.getIntent(), request.getSenderMobile(),
					request.getWalletDestination(), request.getToMember(), request.getAmount(), request.getCurrency(),
					request.getSenderCustomerData().getFirstName(), request.getSenderCustomerData().getSecondName(),
					request.getSenderCustomerData().getIdNumber(), request.getReceiverCustomerData().getFirstName(),
					request.getReceiverCustomerData().getSecondName(), request.getReceiverCustomerData().getIdNumber(),
					"null", request.getReceiverCustomerData().getPhone(), "null", new Date(),
					CommonConstant.WALLET_INCOMING_REMITTANCE_SERVICE, CommonConstant.INITIATE_REQUEST, "null");

			// check for duplicate IssuerTrxRef id
			GimacTransactionLog transctionLog = transactionDao.getTransactionByIssuerTrxRef(request.getIssuerTrxRef());
			if (transctionLog == null) {

				// get last generated Token ID
				GimacAuthentication accessToken = transactionDao.getLastRecordByAccessTokenId();

				if (accessToken != null) {

					// get Config Details
					configMap = systemConfigDao.getConfigDetailsMap();
					
					// log request
					logRequest(request);

					// set service URL
					connectionRequest.setHttpmethodName("POST");
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
					connectionRequest.setServiceUrl(configMap.get(CommonConstant.BASE_URL)
							+ configMap.get(CommonConstant.PAYMENT) + "/" + configMap.get(CommonConstant.SEND));

					// set Headers
					headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + accessToken.getAccessToken());
					connectionRequest.setHeaders(headerMap);

					// log detais in audit table
					walletLogAuditDetails(request.getIssuerTrxRef(), request.getIntent(), request.getSenderMobile(),
							request.getWalletDestination(), request.getToMember(), request.getAmount(),
							request.getCurrency(), request.getSenderCustomerData().getFirstName(),
							request.getSenderCustomerData().getSecondName(),
							request.getSenderCustomerData().getIdNumber(),
							request.getReceiverCustomerData().getFirstName(),
							request.getReceiverCustomerData().getSecondName(),
							request.getReceiverCustomerData().getIdNumber(), "null",
							request.getReceiverCustomerData().getPhone(), "null", new Date(),
							CommonConstant.WALLET_INCOMING_REMITTANCE_SERVICE,
							CommonConstant.INITIATE_REQUEST_TO_PARTNER, "null");
					String req = gson.toJson(request);
					// eventLog
					eventRequestLog(req, request.getIssuerTrxRef());

					LOGGER.info(
							"WalletIncomingRemittanceServiceImpl in walletIncomingService function request sending transfer request with MfsId: "
									+ req);

					// getting gimac response
					// serviceResponse
					// =CallServices.getResponseFromService(connectionRequest,req);
					LOGGER.info(
							"WalletIncomingRemittanceServiceImpl in walletIncomingService function received transfer response for MfsId: "
									+ serviceResponse);
					// dummy response
					serviceResponse = "{\"issuerTrxRef\":\"" + request.getIssuerTrxRef() + "\"," + "\"intent\":\""
							+ request.getIntent() + "\"," + "\"createTime\":\"" + request.getCreateTime() + "\","
							+ "\"senderMobile\":\"" + request.getSenderMobile() + "\"," + "\"walletDestination\":\""
							+ request.getWalletDestination() + "\"," + "\"toMember\":\"" + request.getToMember()
							+ "\",\"description\":\"" + request.getDescription() + "\"," + "\"amount\":\""
							+ request.getAmount() + "\",\"currency\":\"" + request.getCurrency()
							+ "\",\"state\":\"PENDING\"," + "\"voucherCode\":\"VC123\",\"acquirerTrxRef\":\"ATR222\","
							+ "\"senderCustomerData\":{\"firstName\":\""
							+ request.getSenderCustomerData().getFirstName() + "\"," + "\"secondName\":\""
							+ request.getSenderCustomerData().getSecondName() + "\",\"idType\":\""
							+ request.getSenderCustomerData().getIdType() + "\"," + "\"idNumber\":\""
							+ request.getSenderCustomerData().getIdNumber() + "\",\"address\":\""
							+ request.getSenderCustomerData().getAddress() + "\"," + "\"birthDate\":\""
							+ request.getSenderCustomerData().getBirthDate() + "\"},"
							+ "\"receiverCustomerData\":{\"firstName\":\"Ali\","
							+ "\"secondName\":\"BAGHO\",\"idType\":\"passport\","
							+ "\"idNumber\":\"XC145278\",\"address\":\"Road 3,Casablanca\","
							+ "\"city\":\"Casablanca\",\"country\":\"Morocco\",\"phone\":\"+2125224578\","
							+ "\"birthDate\":\"21-09-1981\"}}";

					// String serviceResponse ="{\"error\":\"400 \",\"errorDescription\":\" Error
					// occured\"}";
					eventResponseLog(serviceResponse, request.getIssuerTrxRef(),
							CommonConstant.WALLET_INCOMING_REMITTANCE_SERVICE);
					response = (WalletIncomingResponseDto) JSONToObjectConversion.getObjectFromJson(serviceResponse,
							WalletIncomingResponseDto.class);
					

					// Check for success and fail
					if (response == null) {
						// Null response
						response = new WalletIncomingResponseDto();
						response.setCode(MFSGimacCodes.ER201.getCode());
						response.setMessage(MFSGimacCodes.ER201.getMessage());

					} else if (response.getError() != null) {

						errorLogResponse(request, response);

					} else {

						logResponse(request, response);

					}

				} else {
					// Null Access Toke
					response = new WalletIncomingResponseDto();
					response.setCode(MFSGimacCodes.ER216.getCode());
					response.setMessage(MFSGimacCodes.ER216.getMessage());

				}
			} else {
				// Duplicate record found i.e IssuerTrxRef
				response = new WalletIncomingResponseDto();
				response.setCode(MFSGimacCodes.ER215.getCode());
				response.setMessage(MFSGimacCodes.ER215.getMessage());
			}

			// log audit request and response
			walletLogAuditDetails(request.getIssuerTrxRef(), request.getIntent(), request.getSenderMobile(),
					request.getWalletDestination(), request.getToMember(), request.getAmount(), request.getCurrency(),
					request.getSenderCustomerData().getFirstName(), request.getSenderCustomerData().getSecondName(),
					request.getSenderCustomerData().getIdNumber(), request.getReceiverCustomerData().getFirstName(),
					request.getReceiverCustomerData().getSecondName(), request.getReceiverCustomerData().getIdNumber(),
					request.getReceiverCustomerData().getPhone(), response.getState(), response.getVoucherCode(),
					new Date(), CommonConstant.WALLET_INCOMING_REMITTANCE_SERVICE, CommonConstant.RESPONSE_FROM_PARTNER,
					response.getMessage());

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in WalletIncomingRemittanceServiceImpl" + de);
			response = new WalletIncomingResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in WalletIncomingRemittanceServiceImpl" + e);
			response = new WalletIncomingResponseDto();
			response.setCode(MFSGimacCodes.ER207.getCode());
			response.setMessage(MFSGimacCodes.ER207.getMessage());
		}

		return response;
	}

	// event response log
	private void eventResponseLog(String response, String issuerTrxRef, String servicename) throws DaoException {

		GimacEventLog transactionEventLog = transactionDao.getByIssureTrxRefAndServiceName(issuerTrxRef, servicename);

		if (transactionEventLog != null) {
			transactionEventLog.setResponse(response);
			transactionEventLog.setDateLogged(new Date());
			transactionDao.update(transactionEventLog);

		}

	}

	// event request log
	private void eventRequestLog(String request, String issuerTrxRef) throws DaoException {

		GimacEventLog eventLog = new GimacEventLog();

		eventLog.setMfsId(issuerTrxRef);
		eventLog.setRequest(request);
		eventLog.setServiceName(CommonConstant.WALLET_INCOMING_REMITTANCE_SERVICE);
		eventLog.setDateLogged(new Date());
		transactionDao.save(eventLog);

	}

	// log error request response
	private void errorLogResponse(WalletIncomingRequestDto request, WalletIncomingResponseDto response)
			throws DaoException {

		GimacTransactionError transactionErrorLog = new GimacTransactionError();

		transactionErrorLog.setIssuerTrxRef(request.getIssuerTrxRef());
		transactionErrorLog.setIntent(request.getIntent());
		transactionErrorLog.setSenderMobile(request.getSenderMobile());
		transactionErrorLog.setAccountNumber(request.getWalletDestination());
		transactionErrorLog.setToMember(request.getToMember());
		transactionErrorLog.setAmount(request.getAmount());
		transactionErrorLog.setCurrency(request.getCurrency());
		transactionErrorLog.setSenderFirstName(request.getSenderCustomerData().getFirstName());
		transactionErrorLog.setSenderSecondName(request.getSenderCustomerData().getSecondName());
		transactionErrorLog.setSenderIdNumber(request.getSenderCustomerData().getIdNumber());
		transactionErrorLog.setReceiverFirstName(request.getReceiverCustomerData().getFirstName());
		transactionErrorLog.setReceiverSecondName(request.getReceiverCustomerData().getSecondName());
		transactionErrorLog.setReceiverIdNumber(request.getReceiverCustomerData().getIdNumber());
		transactionErrorLog.setState(response.getState());
		transactionErrorLog.setVoucherCode(response.getVoucherCode());
		transactionErrorLog.setAcquirerTrxRef(response.getAcquirerTrxRef());
		transactionErrorLog.setServiceName("WalletIncomingRemittance");
		transactionErrorLog.setDateLogged(new Date());
		transactionErrorLog.setError(response.getError());
		transactionErrorLog.setErrorDescription(response.getErrorDescription());

		transactionDao.save(transactionErrorLog);

	}

	// log response
	private void logResponse(WalletIncomingRequestDto request, WalletIncomingResponseDto response) throws DaoException {
		GimacTransactionLog transactionLog = transactionDao.getTransactionByIssuerTrxRef(request.getIssuerTrxRef());
		if (transactionLog != null) {
			transactionLog.setIntent(response.getIntent());
			transactionLog.setCreateTime(response.getCreateTime());
			transactionLog.setSenderMobile(response.getSenderMobile());
			transactionLog.setWalletDestination(response.getWalletDestination());
			transactionLog.setToMember(response.getToMember());
			transactionLog.setDescription(response.getDescription());
			transactionLog.setAmount(response.getAmount());
			transactionLog.setCurrency(response.getCurrency());
			transactionLog.setState(response.getState());
			transactionLog.setVoucherCode(response.getVoucherCode());
			transactionLog.setAcquirerTrxRef(response.getAcquirerTrxRef());
			transactionLog.setSenderFirstName(response.getSenderCustomerData().getFirstName());
			transactionLog.setSenderSecondName(response.getSenderCustomerData().getSecondName());
			transactionLog.setSenderIdType(response.getSenderCustomerData().getIdType());
			transactionLog.setSenderIdNumber(response.getSenderCustomerData().getIdNumber());
			transactionLog.setSenderAddress(response.getSenderCustomerData().getAddress());
			transactionLog.setSenderBirthDate(response.getSenderCustomerData().getBirthDate());
			transactionLog.setReceiverFirstName(response.getReceiverCustomerData().getFirstName());
			transactionLog.setReceiverSecondName(response.getReceiverCustomerData().getSecondName());
			transactionLog.setReceiverIdType(response.getReceiverCustomerData().getIdType());
			transactionLog.setReceiverIdNumber(response.getReceiverCustomerData().getIdNumber());
			transactionLog.setReceiverAddress(response.getReceiverCustomerData().getAddress());
			transactionLog.setReceiverCity(response.getReceiverCustomerData().getCity());
			transactionLog.setReceiverCountry(response.getReceiverCustomerData().getCountry());
			transactionLog.setReceiverPhone(response.getReceiverCustomerData().getPhone());
			transactionLog.setReceiverBirthDate(response.getReceiverCustomerData().getBirthDate());
			transactionLog.setServiceName("WalletIncomingRemittance");
			transactionLog.setError(response.getError());
			transactionLog.setErrorDescription(response.getErrorDescription());
			transactionLog.setDateLogged(new Date());
			transactionDao.update(transactionLog);
		}
	}

	// log request
	private void logRequest(WalletIncomingRequestDto request) throws DaoException {

		GimacTransactionLog transactionLog = new GimacTransactionLog();

		transactionLog.setIssuerTrxRef(request.getIssuerTrxRef());
		transactionLog.setIntent(request.getIntent());
		transactionLog.setCreateTime(request.getCreateTime());
		transactionLog.setSenderMobile(request.getSenderMobile());
		transactionLog.setWalletDestination(request.getWalletDestination());
		transactionLog.setToMember(request.getToMember());
		transactionLog.setAmount(request.getAmount());
		transactionLog.setCurrency(request.getCurrency());
		transactionLog.setSenderFirstName(request.getSenderCustomerData().getFirstName());
		transactionLog.setSenderSecondName(request.getSenderCustomerData().getSecondName());
		transactionLog.setSenderIdType(request.getSenderCustomerData().getIdType());
		transactionLog.setSenderIdNumber(request.getSenderCustomerData().getIdNumber());
		transactionLog.setSenderAddress(request.getSenderCustomerData().getAddress());
		transactionLog.setSenderBirthDate(request.getSenderCustomerData().getBirthDate());
		transactionLog.setReceiverFirstName(request.getReceiverCustomerData().getFirstName());
		transactionLog.setReceiverSecondName(request.getReceiverCustomerData().getSecondName());
		transactionLog.setReceiverIdType(request.getReceiverCustomerData().getIdType());
		transactionLog.setReceiverIdNumber(request.getReceiverCustomerData().getIdNumber());
		transactionLog.setReceiverAddress(request.getReceiverCustomerData().getAddress());
		transactionLog.setReceiverCity(request.getReceiverCustomerData().getCity());
		transactionLog.setReceiverCountry(request.getReceiverCustomerData().getCountry());
		transactionLog.setReceiverPhone(request.getReceiverCustomerData().getPhone());
		transactionLog.setReceiverBirthDate(request.getReceiverCustomerData().getBirthDate());
		transactionLog.setServiceName("WalletIncomingRemittance");
		transactionLog.setDateLogged(new Date());

		transactionDao.save(transactionLog);
	}

}