package com.mfs.client.gimac.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.gimac.dao.SystemConfigDao;
import com.mfs.client.gimac.dao.TransactionDao;
import com.mfs.client.gimac.dto.WalletIncomingCancellationRequestDto;
import com.mfs.client.gimac.dto.WalletIncomingCancellationResponseDto;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacAuthentication;
import com.mfs.client.gimac.models.GimacEventLog;
import com.mfs.client.gimac.models.GimacTransactionError;
import com.mfs.client.gimac.models.GimacTransactionLog;
import com.mfs.client.gimac.service.WalletIncomingCancellationService;
import com.mfs.client.gimac.util.CallServices;
import com.mfs.client.gimac.util.CommonConstant;
import com.mfs.client.gimac.util.ExpiredToken;
import com.mfs.client.gimac.util.HttpsConnectionRequest;
import com.mfs.client.gimac.util.HttpsConnectionResponse;
import com.mfs.client.gimac.util.HttpsConnectorImpl;
import com.mfs.client.gimac.util.JSONToObjectConversion;
import com.mfs.client.gimac.util.MFSGimacCodes;

/*
* @author Priyanka Prakash Khade
* WalletIncomingCancellationService.java
* 26-05-2020
*
* purpose: this class is used for WalletIncomingCancellationService
*
* Methods
* 1. walletCancellation
* 2. log request in audit
* 2. log response
* 3. log error request and response
*
*/
@Service("WalletIncomingCancellationService")
public class WalletIncomingCancellationServiceImpl extends BaseAuditServiceImpl
		implements WalletIncomingCancellationService {

	private static final Logger LOGGER = Logger.getLogger(WalletIncomingCancellationServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDao systemConfigDao;

	public WalletIncomingCancellationResponseDto walletCancellation(WalletIncomingCancellationRequestDto request) {

		Map<String, String> headerMap = new HashMap<String, String>();
		Map<String, String> configMap = null;
		Gson gson = new Gson();
		HttpsConnectionResponse httpsConResult = null;
		String serviceResponse = null;
		WalletIncomingCancellationResponseDto response = new WalletIncomingCancellationResponseDto();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();

		try {

			// Log request in audit table
			walletCancellationlogAudit(request.getIssuerTrxRef(), request.getIntent(), request.getState(),
					request.getVoucherCode(), CommonConstant.WALLET_INCOMING_CANCELLATION_SERVICE, new Date(),
					CommonConstant.INITIATE_REQUEST, "null");

			// get data of issureTrxRef from db
			GimacTransactionLog transactionLog = transactionDao.getTransactionByIssuerTrxRef(request.getIssuerTrxRef());

			if (transactionLog != null) {

				// get last generated Token ID
				GimacAuthentication accessToken = transactionDao.getLastRecordByAccessTokenId();
				if (accessToken != null) {

					// get Config Details
					configMap = systemConfigDao.getConfigDetailsMap();

					// check for status
					if (transactionLog.getState().equalsIgnoreCase(configMap.get(CommonConstant.STATUS))) {

						// set service URL
						connectionRequest.setHttpmethodName("POST");
						connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
						connectionRequest.setServiceUrl(configMap.get(CommonConstant.BASE_URL)
								+ configMap.get(CommonConstant.PAYMENT) + "/" + configMap.get(CommonConstant.UPDATE));
						// set Headers
						headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
						connectionRequest.setHeaders(headerMap);
						// getting gimac response
						walletCancellationlogAudit(request.getIssuerTrxRef(), request.getIntent(), request.getState(),
								request.getVoucherCode(), CommonConstant.WALLET_INCOMING_CANCELLATION_SERVICE,
								new Date(), CommonConstant.INITIATE_REQUEST_TO_PARTNER, "null");
						String req = gson.toJson(request);

						// eventLog
						eventRequestLog(req, request.getIssuerTrxRef());

						LOGGER.info(
								"WalletIncomingCancellationServiceImpl in walletCancellation function request " + req);
						// getting gimac response
						// serviceResponse
						// =CallServices.getResponseFromService(connectionRequest,req);
						LOGGER.info(
								" WalletIncomingCancellationServiceImpl in WalletIncomingCancellationService function response "
										+ serviceResponse);
						serviceResponse = "{\"issuerTrxRef\":\"" + request.getIssuerTrxRef() + "\","
								+ "\"voucherCode\":\"" + request.getVoucherCode() + "\","
								+ "\"state\":\"CANCELLED\",\"updateTime\":\"1490109044\"}";
						eventResponseLog(serviceResponse);

						response = (WalletIncomingCancellationResponseDto) JSONToObjectConversion
								.getObjectFromJson(serviceResponse, WalletIncomingCancellationResponseDto.class);

						// Check for success and fail
						if (response != null) {

							logResponse(request, response);

						} else {
							// Null response
							response = new WalletIncomingCancellationResponseDto();
							response.setCode(MFSGimacCodes.ER201.getCode());
							response.setMessage(MFSGimacCodes.ER201.getMessage());
						}
					} else {
						// Transaction is cancelled
						response = new WalletIncomingCancellationResponseDto();
						response.setCode(MFSGimacCodes.ER217.getCode());
						response.setMessage(MFSGimacCodes.ER217.getMessage());
					}
				} else {
					// Null Access Token
					response = new WalletIncomingCancellationResponseDto();
					response.setCode(MFSGimacCodes.ER216.getCode());
					response.setMessage(MFSGimacCodes.ER216.getMessage());
				}
			} else {
				// No record found against issureTrxRef
				response = new WalletIncomingCancellationResponseDto();
				response.setCode(MFSGimacCodes.ER218.getCode());
				response.setMessage(MFSGimacCodes.ER218.getMessage());

			}
			walletCancellationlogAudit(request.getIssuerTrxRef(), request.getIntent(), response.getState(),
					request.getVoucherCode(), CommonConstant.WALLET_INCOMING_CANCELLATION_SERVICE, new Date(),
					CommonConstant.RESPONSE_FROM_PARTNER, response.getMessage());

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in WalletIncomingCancellationServiceImpl " + de);
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in WalletIncomingCancellationServiceImpl" + e);
			response.setCode(MFSGimacCodes.ER207.getCode());
			response.setMessage(MFSGimacCodes.ER207.getMessage());
		}
		return response;
	}

	private void eventResponseLog(String response) throws DaoException {

		GimacEventLog transactionEventLog = transactionDao.getByLastRecord();

		if (transactionEventLog != null) {
			transactionEventLog.setResponse(response);
			transactionEventLog.setDateLogged(new Date());
			transactionDao.update(transactionEventLog);

		}
	}

	private void eventRequestLog(String request, String issuerTrxRef) throws DaoException {

		GimacEventLog eventLog = new GimacEventLog();

		eventLog.setMfsId(issuerTrxRef);
		eventLog.setRequest(request);
		eventLog.setServiceName(CommonConstant.WALLET_INCOMING_CANCELLATION_SERVICE);
		eventLog.setDateLogged(new Date());
		transactionDao.save(eventLog);
	}

	// update request and response
	private void logResponse(WalletIncomingCancellationRequestDto request,
			WalletIncomingCancellationResponseDto response) throws DaoException {

		GimacTransactionLog transactionLog = transactionDao.getTransactionByIssuerTrxRef(request.getIssuerTrxRef());
		if (transactionLog != null) {
			transactionLog.setIssuerTrxRef(request.getIssuerTrxRef());
			transactionLog.setIntent(request.getIntent());
			transactionLog.setUpdateTime(response.getUpdateTime());
			transactionLog.setVoucherCode(request.getVoucherCode());
			transactionLog.setState(response.getState());
			transactionLog.setServiceName("WalletRemittanceCancelltion");
			transactionLog.setDateLogged(new Date());
			transactionDao.update(transactionLog);
		}

	}

}