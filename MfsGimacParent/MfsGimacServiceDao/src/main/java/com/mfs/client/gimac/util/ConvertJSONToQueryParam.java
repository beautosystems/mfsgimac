package com.mfs.client.gimac.util;

public class ConvertJSONToQueryParam {
	public static String getQueryParam(String json_input) {
		json_input=json_input.replace(":","=");
		json_input=json_input.replace(",","&");
		json_input=json_input.replace("\"","");
		json_input=json_input.replace("{","");
		json_input=json_input.replace("}","");
		json_input=json_input.replace("\\n","");
		json_input=json_input.replace(" ","");
		
		json_input=json_input.replace("login=","");
		return json_input;
	}
}
