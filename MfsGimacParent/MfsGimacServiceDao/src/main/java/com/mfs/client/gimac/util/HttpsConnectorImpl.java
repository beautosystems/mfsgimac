package com.mfs.client.gimac.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import org.apache.log4j.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class HttpsConnectorImpl {

	private static final int CHECK_RESPONSE_CODE201 = 201;
	private static final int CHECK_RESPONSE_CODE200 = 200;
	private static final int CHECK_RESPONSE_CODE202 = 202;
	private static final Logger LOGGER = Logger.getLogger(HttpConnectorImpl.class);

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	public HttpsConnectionResponse connectionUsingHTTPS(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException, Exception {
		SSLContext sslContext = null;
		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();

		try {
			sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {
			throw new Exception(e);
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		// Read timeout Changed to 60 sec
		httpsConnection.setReadTimeout(60000);

		// Send post request
		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {
			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();
		LOGGER.info("function response code" + responseCode);
		System.out.println("responseCode:" + responseCode);

		BufferedReader httpsResponse = null;

		if (responseCode == GimacResponseCode.S201.getCode() || responseCode == GimacResponseCode.S200.getCode()
				|| responseCode == GimacResponseCode.S202.getCode()) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else if (responseCode == GimacResponseCode.ER400.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.INVALID_REQUEST.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.INVALID_REQUEST.getMessage() + errMessage);

			System.out.println("Error Message :" + getInputAsString(httpsConnection.getErrorStream()));

			LOGGER.info("Error Message :" + getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == GimacResponseCode.ER401.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.AUTHENTICATION_FAILURE.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.AUTHENTICATION_FAILURE.getMessage() + errMessage);

			System.out.println("Error Message :" + getInputAsString(httpsConnection.getErrorStream()));

			LOGGER.info("Error Message f:" + getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == GimacResponseCode.ER403.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.NOT_AUTHORIZED.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.NOT_AUTHORIZED.getMessage() + errMessage);

		} else if (responseCode == GimacResponseCode.ER404.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.RESOURCE_NOT_FOUND.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.RESOURCE_NOT_FOUND.getMessage() + errMessage);

		} else if (responseCode == GimacResponseCode.ER405.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.METHOD_NOT_SUPPORTED.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.METHOD_NOT_SUPPORTED.getMessage() + errMessage);

		} else if (responseCode == GimacResponseCode.ER406.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.MEDIA_TYPE_NOT_ACCEPTABLE.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.MEDIA_TYPE_NOT_ACCEPTABLE.getMessage() + errMessage);

		} else if (responseCode == GimacResponseCode.ER415.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.UNSUPPORTED_MEDIA_TYPE.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.UNSUPPORTED_MEDIA_TYPE.getMessage() + errMessage);

		} else if (responseCode == GimacResponseCode.ER422.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.UNPROCCESSABLE_ENTITY.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.UNPROCCESSABLE_ENTITY.getMessage() + errMessage);

		} else if (responseCode == GimacResponseCode.ER500.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.INTERNAL_SERVER_ERROR.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.INTERNAL_SERVER_ERROR.getMessage() + errMessage);

			System.out.println("err" + getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == GimacResponseCode.ER503.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			connectionResponse.setCode(MFSGimacCodes.SERVICE_UNAVAILABLE.getCode());

			connectionResponse.setTxMessage(MFSGimacCodes.SERVICE_UNAVAILABLE.getMessage() + errMessage);
		} else {

			if (httpsConnection != null) {
				httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));
			}

		}

		String output = null;
		if (httpsResponse != null) {
			output = readConnectionDataWithBuffer(httpsResponse);
		}

		if (httpsResponse != null) {
			httpsResponse.close();
		}

		connectionResponse.setResponseData(output);

		return connectionResponse;
	}

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	private BufferedReader connectionUsingHTTPSRetry(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException {
		SSLContext sslContext = null;

		try {
			sslContext = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
		}
		try {
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		// Read timeout Changed to 60 sec
		httpsConnection.setReadTimeout(60000);

		// Send post request
		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {

			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();

		BufferedReader httpsResponse = null;

		if (responseCode == CHECK_RESPONSE_CODE200 || responseCode == CHECK_RESPONSE_CODE201
				|| responseCode == CHECK_RESPONSE_CODE202) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));

		}

		String output = readConnectionDataWithBuffer(httpsResponse);

		if (output == null || output.isEmpty()) {
			if (responseCode == CHECK_RESPONSE_CODE200 || responseCode == CHECK_RESPONSE_CODE201
					|| responseCode == CHECK_RESPONSE_CODE202) {
				output = "OK";
			}
		}

		return httpsResponse;
	}

	public static void wait(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
		}
	}

	/**
	 * @param httpsResponse
	 * @return
	 * @throws IOException
	 */
	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}

	/**
	 * Set Header Properties To HTTPS Connection
	 * 
	 * @param httpsConnectionRequest
	 * @param connectionData
	 * @param httpsConnection
	 */
	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpsURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		} else {
			httpsConnection.setRequestProperty("Content-Length", "0");
		}

	}

	/**
	 * Set headers that are required by calling app
	 * 
	 * @param headers
	 * @param httpsConnection
	 */
	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpsURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
	}

	private static String getInputAsString(InputStream paramInputStream) {
		Scanner localScanner = new Scanner(paramInputStream);
		return localScanner.useDelimiter("\\A").hasNext() ? localScanner.next() : "";
	}

}
