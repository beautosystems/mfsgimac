package com.mfs.client.gimac.util;

public enum GimacResponseCode
{
	S200(200 , "OK"),
	S201(201 , "Created"),
	S202(202 , "Accepted"),
	ER400(400 ,"Bad Request"),
	ER401(401 , "Unauthorized"),
	ER403(403 , "Not_Authorized"),
	ER404(404 , "Resource not found"),
	ER405(405 , "Method not supported"),
	ER406(406 , "Media type not acceptable"),
	ER415(415 , "Unsupported media type"),
	ER422(422 , "Unproccessable entity"),
	ER500(500 , "Internal server error"),
	ER503(503 , "Service Unavailable");
	
	private int code;
	private String message;

	private GimacResponseCode(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
