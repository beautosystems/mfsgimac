package com.mfs.client.gimac.dto;

import java.util.List;

public class PaymentInquiryRequestDto {
	private String asIssuer;
	private String asAcquirer;
	private String firstElement;
	private String pageSize;
	private String acquirerTrxRef;
	private String fromMember;
	private String toMember;
	private String fromDate;
	private String toDate;
	private String issuerTrxRef;
	private String voucherCode;
	private List<String> status;
	private List<String> intents;

	public String getAsIssuer() {
		return asIssuer;
	}

	public void setAsIssuer(String asIssuer) {
		this.asIssuer = asIssuer;
	}

	public String getAsAcquirer() {
		return asAcquirer;
	}

	public void setAsAcquirer(String asAcquirer) {
		this.asAcquirer = asAcquirer;
	}

	public String getFirstElement() {
		return firstElement;
	}

	public void setFirstElement(String firstElement) {
		this.firstElement = firstElement;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getAcquirerTrxRef() {
		return acquirerTrxRef;
	}

	public void setAcquirerTrxRef(String acquirerTrxRef) {
		this.acquirerTrxRef = acquirerTrxRef;
	}

	public String getFromMember() {
		return fromMember;
	}

	public void setFromMember(String fromMember) {
		this.fromMember = fromMember;
	}

	public String getToMember() {
		return toMember;
	}

	public void setToMember(String toMember) {
		this.toMember = toMember;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}

	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}

	public List<String> getIntents() {
		return intents;
	}

	public void setIntents(List<String> intents) {
		this.intents = intents;
	}

	@Override
	public String toString() {
		return "PaymentInquiryRequestDto [asIssuer=" + asIssuer + ", asAcquirer=" + asAcquirer + ", firstElement="
				+ firstElement + ", pageSize=" + pageSize + ", acquirerTrxRef=" + acquirerTrxRef + ", fromMember="
				+ fromMember + ", toMember=" + toMember + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", issuerTrxRef=" + issuerTrxRef + ", voucherCode=" + voucherCode + ", status=" + status
				+ ", intents=" + intents + "]";
	}

}
