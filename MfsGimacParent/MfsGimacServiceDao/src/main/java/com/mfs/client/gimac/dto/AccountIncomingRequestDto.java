package com.mfs.client.gimac.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class AccountIncomingRequestDto {
	
	private String issuerTrxRef;
	private String intent;
	private String createTime;
	private String senderMobile;
	private String accountNumber;
	private String toMember;
	private String description;
	private double amount;
	private String currency;
	private SenderCustomerDataDto senderCustomerData;
	private ReceiverCustomerDataDto receiverCustomerData;

	public String getIssuerTrxRef() {
		return issuerTrxRef;
	}
	public void setIssuerTrxRef(String issuerTrxRef) {
		this.issuerTrxRef = issuerTrxRef;
	}
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getSenderMobile() {
		return senderMobile;
	}
	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getToMember() {
		return toMember;
	}
	public void setToMember(String toMember) {
		this.toMember = toMember;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public SenderCustomerDataDto getSenderCustomerData() {
		return senderCustomerData;
	}
	public void setSenderCustomerData(SenderCustomerDataDto senderCustomerData) {
		this.senderCustomerData = senderCustomerData;
	}
	public ReceiverCustomerDataDto getReceiverCustomerData() {
		return receiverCustomerData;
	}
	public void setReceiverCustomerData(ReceiverCustomerDataDto receiverCustomerData) {
		this.receiverCustomerData = receiverCustomerData;
	}
	
	@Override
	public String toString() {
		return "AccountIncomingRequestDto [issuerTrxRef=" + issuerTrxRef + ", intent=" + intent + ", createTime="
				+ createTime + ", senderMobile=" + senderMobile + ", accountNumber=" + accountNumber + ", toMember="
				+ toMember + ", description=" + description + ", amount=" + amount + ", currency=" + currency
				+ ", senderCustomerData=" + senderCustomerData + ", receiverCustomerData=" + receiverCustomerData + "]";
	}
	
	
	

}
