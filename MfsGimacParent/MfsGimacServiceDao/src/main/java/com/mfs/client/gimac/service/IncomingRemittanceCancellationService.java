package com.mfs.client.gimac.service;

import com.mfs.client.gimac.dto.RemittanceCancellationRequestDto;
import com.mfs.client.gimac.dto.RemittanceCancellationResponseDto;

public interface IncomingRemittanceCancellationService {
	
	RemittanceCancellationResponseDto incomingRemittanceCancellation(RemittanceCancellationRequestDto request);

}
