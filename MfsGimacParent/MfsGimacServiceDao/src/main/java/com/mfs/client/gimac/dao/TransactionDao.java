package com.mfs.client.gimac.dao;

import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacAuthentication;
import com.mfs.client.gimac.models.GimacEventLog;
import com.mfs.client.gimac.models.GimacPaymentInquiry;
import com.mfs.client.gimac.models.GimacTransactionLog;

public interface TransactionDao extends BaseDao{

	GimacTransactionLog getTransactionByIssuerTrxRef(String issuerTrxRef) throws DaoException ;

	GimacAuthentication getLastRecordByAccessTokenId();

	GimacPaymentInquiry getPaymentInquiryByIssuerTrxRef(String issuerTrxRef) throws DaoException;
	
	GimacEventLog getByIssureTrxRefAndServiceName(String issureTrxRef,String serviceName);

	GimacEventLog getByLastRecord();

}
