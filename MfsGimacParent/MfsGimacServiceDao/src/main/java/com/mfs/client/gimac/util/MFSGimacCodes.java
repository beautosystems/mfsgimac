package com.mfs.client.gimac.util;

public enum MFSGimacCodes {
	
	ER201("ER201","Null response"),
	ER204("ER204","Exception while save data"),
	ER205("ER205","Exception while update"),
	ER202("ER202","Exception while getting TransactionLog by issuerTrxRef."),

	S200("200","Transaction Success"),
	ER400("ER400","Transaction rejected"),
	ER401("ER401","Bad or expired access token"),
	ER403("ER403","Access forbidden"),
	ER500("ER500","internal error"),
	ER206("206","Transaction doesn't exist against ExTrId"),
	ER207("ER207","System Error"),
	ER208("ER208", "Error while getting last record"),
	ER210("ER210","No record found against externalRefId and serviceName"),
	ER214("ER214","Mobile number is not register"),
	ER215("ER215","Duplicate record found i.e IssuerTrxRef"),
	ER216("ER216", "Null Access Token"),
	ER217("ER217","Transaction is cancelled"),
	ER218("ER218","No record found against issureTrxRef"),
	
	INVALID_REQUEST("INVALID_REQUEST", "Request is not well-formed, syntactically incorrect, or violates schema."),
	AUTHENTICATION_FAILURE("AUTHENTICATION_FAILURE",
			"Authentication failed due to invalid authentication credentials."),
	NOT_AUTHORIZED("NOT_AUTHORIZED", "Authorization failed due to insufficient permissions."),
	RESOURCE_NOT_FOUND("RESOURCE_NOT_FOUND", "The specified resource does not exist."),
	METHOD_NOT_SUPPORTED("METHOD_NOT_SUPPORTED", "he server does not implement the requested HTTP method."),
	MEDIA_TYPE_NOT_ACCEPTABLE("MEDIA_TYPE_NOT_ACCEPTABLE",
			"The server does not implement the media type that would be acceptable to the client."),
	UNSUPPORTED_MEDIA_TYPE("UNSUPPORTED_MEDIA_TYPE", "The server does not support the request payload’s media type."),
	UNPROCCESSABLE_ENTITY("UNPROCCESSABLE_ENTITY",
			"The API cannot complete the requested action, or the request action is semantically incorrect or fails business validation."),
	INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR", "An internal server error has occurred."),
	SERVICE_UNAVAILABLE("SERVICE_UNAVAILABLE", "Service Unavailable."),
	TOKEN_EXPIRED("TOKEN_EXPIRED","Token Expired"),
	INVALID_ISSUERTRXREF("INVALID_ISSUERTRXREF","Provide correct issuertrxref"),
	INVALID_INTENT("INVALID_INTENT","Invalid transaction type"), 
	INVALID_AMOUNT("INVALID_AMOUNT","Please enter amount greater than 0"),
	INVALID_TOMEMBER("INVALID_TOMEMBER","Provide correct toMember"),
	INVALID_ACCOUNT_NUMBER("INVALID_ACCOUNT_NUMBER","Provide correct account number"),
	INVALID_CURRENCY("INVALID_CURRENCY","provide correct currency code"),
	INVALID_VOUCHER_CODE("INVALID_VOUCHER_CODE","Provide correct voucher code"),
	INVALID_STATE("INVALID_STATE","Invalid state"),
	INVALID_UPDATE_TIME("INVALID_UPDATE_TIME","Invalid update time"), 
	INVALID_WALLETDESTINATION("INVALID_WALLETDESTINATION","Invalid wallet destination"),
	NULL_REQUEST("NULL_REQUEST","Request cannot be null"); 
	
    private String code;
	private String message;

	private MFSGimacCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}
	
	

	public String getMessage() {
		return message;
	}


}
