package com.mfs.client.gimac.service;

import com.mfs.client.gimac.dto.ListPaymentInquiryResponseDto;
import com.mfs.client.gimac.dto.PaymentInquiryRequestDto;
import com.mfs.client.gimac.dto.PaymentInquiryResponseDto;

public interface PaymentInquiryService {
	
	ListPaymentInquiryResponseDto paymentInquiryService(PaymentInquiryRequestDto request);

}
