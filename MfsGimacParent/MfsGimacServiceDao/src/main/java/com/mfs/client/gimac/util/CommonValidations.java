package com.mfs.client.gimac.util;

import java.util.regex.Pattern;

public class CommonValidations {

	public boolean validateStringValues(String reqParamValue) {
		boolean validateResult = false;
		String request = reqParamValue.trim();
		if (request == null || request.equals(""))
			validateResult = true;

		return validateResult;
	}

	public boolean validateAmountValues(Double amount) {
		boolean validateResult = false;
		if (amount == null)
			validateResult = true;

		return validateResult;
	}

	public boolean validateNumber(String number) {
		boolean validateResult = false;
		if (number == null || !number.matches("[0-9]+"))
			validateResult = true;
		return validateResult;

	}

	public boolean isStringOnlyAlphabet(String str) {
		boolean validateResult = false;
		if ((str.equals("")) || (str == null) || (!str.matches("^[ A-Za-z0-9_@./#&+-]*$")))

			validateResult = true;

		return validateResult;
	}


	public  boolean validateString(String reqParamValue) {
		boolean validateResult = false;
		Pattern pattern = Pattern.compile("^[ A-Za-z_@./#&+-]*$");
		if (pattern.matcher(reqParamValue).matches())
			validateResult = true;

		return validateResult;
	}
	
	
}
