package com.mfs.client.gimac.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mfs.client.gimac.service.impl.AuthenticationServiceImpl;

public class CallServices {

	public static String getResponseFromService(HttpsConnectionRequest connectionRequest, String request)
			throws Exception {

		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;

		try {

			Map<String, String> headerMap = new HashMap<String, String>();

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);

			connectionRequest.setHeaders(headerMap);

			// boolean isHttps = true;
			if (connectionRequest.isHttps()) {

				connectionRequest.setPort(connectionRequest.getPort());
				HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
			} else {

				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				httpsConResult = httpConnectorImpl.httpUrlConnection(connectionRequest, request);
			}

			str_result = httpsConResult.getResponseData();

		} catch (Exception e) {

			throw new Exception(e);
		}
		return str_result;
	}

	public static String getResponseFromService(String url, String signature) throws Exception {

		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;

		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();

			Map<String, String> headerMap = new HashMap<String, String>();

		//	headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
			// headerMap.put(CommonConstant.AUTHORIZATION, signature);

			connectionRequest.setServiceUrl(url);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");

			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			httpsConResult = httpConnectorImpl.httpUrlConnection(connectionRequest, signature);
			str_result = httpsConResult.getResponseData();

		} catch (Exception e) {
			throw new Exception(e);
		}
		return str_result;
	}
	public static String getResponseFromService1(String url, String request) {
		final Logger LOGGER = Logger.getLogger(AuthenticationServiceImpl.class);
			LOGGER.info("Request -> "+request);
			String queryParam = ConvertJSONToQueryParam.getQueryParam(request);
			
			
			
			String response = null;
			try {
				//Updated code for 
				//Http and https switch
				//use same generic http functions
				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				
				HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
				connectionRequest.setServiceUrl(url);
				connectionRequest.setHttpmethodName("POST");
				
				Map<String, String> headerMap = new HashMap<String, String>();
				headerMap.put("charset", "utf-8");
				headerMap.put("Content-Type", "application/x-www-form-urlencoded");
				
				connectionRequest.setHeaders(headerMap);
				
				HttpsConnectionResponse connectionResponse = httpConnectorImpl.httpUrlConnection(connectionRequest, queryParam);
				
				response = connectionResponse.getResponseData();
			} catch (Exception e) {
				LOGGER.error(e);

			}
			return response;
		}
	
}
