package com.mfs.client.gimac.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class ListPaymentInquiryResponseDto {
	  private List<PaymentInquiryResponseDto> paymentInquiry;
	  private String error;
	  private String error_description;
	  private String code;
	  private String message;
	public List<PaymentInquiryResponseDto> getPaymentInquiry() {
		return paymentInquiry;
	}
	public void setPaymentInquiry(List<PaymentInquiryResponseDto> paymentInquiry) {
		this.paymentInquiry = paymentInquiry;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getError_description() {
		return error_description;
	}
	public void setError_description(String error_description) {
		this.error_description = error_description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "ListPaymentInquiryResponseDto [paymentInquiry=" + paymentInquiry + ", error=" + error
				+ ", error_description=" + error_description + ", code=" + code + ", message=" + message + "]";
	}
	  
	  
	
}
