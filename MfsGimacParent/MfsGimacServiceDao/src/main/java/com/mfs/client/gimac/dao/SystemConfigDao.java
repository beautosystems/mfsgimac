package com.mfs.client.gimac.dao;

import java.util.Map;

import com.mfs.client.gimac.exception.DaoException;

public interface SystemConfigDao {

	public Map<String, String> getConfigDetailsMap() throws DaoException;
	
}
