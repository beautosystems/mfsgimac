package com.mfs.client.gimac.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class HttpsConnectionResponse {

	private String responseData;

	private int respCode;

	private String code;

	private String txStatus;

	private String txMessage;
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTxStatus() {
		return txStatus;
	}

	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}

	

	public String getTxMessage() {
		return txMessage;
	}

	public void setTxMessage(String txMessage) {
		this.txMessage = txMessage;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	public int getRespCode() {
		return respCode;
	}

	public void setRespCode(int respCode) {
		this.respCode = respCode;
	}

	@Override
	public String toString() {
		return "HttpsConnectionResponse [responseData=" + responseData + ", respCode=" + respCode + ", code=" + code
				+ ", txStatus=" + txStatus + ", txMessage=" + txMessage + "]";
	}

}
