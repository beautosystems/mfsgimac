package com.mfs.client.gimac.service.impl;

import java.util.Date;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.reflect.Type;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfs.client.gimac.dao.SystemConfigDao;
import com.mfs.client.gimac.dao.TransactionDao;
import com.mfs.client.gimac.dto.AccountIncomingResponseDto;
import com.mfs.client.gimac.dto.ListPaymentInquiryResponseDto;
import com.mfs.client.gimac.dto.PaymentInquiryRequestDto;
import com.mfs.client.gimac.dto.PaymentInquiryResponseDto;
import com.mfs.client.gimac.exception.DaoException;
import com.mfs.client.gimac.models.GimacAuthentication;
import com.mfs.client.gimac.models.GimacEventLog;
import com.mfs.client.gimac.models.GimacPaymentInquiry;
import com.mfs.client.gimac.service.PaymentInquiryService;
import com.mfs.client.gimac.util.CallServices;
import com.mfs.client.gimac.util.CommonConstant;
import com.mfs.client.gimac.util.ExpiredToken;
import com.mfs.client.gimac.util.HttpsConnectionRequest;
import com.mfs.client.gimac.util.HttpsConnectionResponse;
import com.mfs.client.gimac.util.HttpsConnectorImpl;
import com.mfs.client.gimac.util.MFSGimacCodes;

@Service("PaymentInquiryService")
public class PaymentInquiryServiceImpl extends BaseAuditServiceImpl implements PaymentInquiryService {
	private static final Logger LOGGER = Logger.getLogger(PaymentInquiryServiceImpl.class);

	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	// This Function is use to call Payment Inquiry
	public ListPaymentInquiryResponseDto paymentInquiryService(PaymentInquiryRequestDto request) {
		Map<String, String> headerMap = new HashMap<String, String>();
		Gson gson = new Gson();
		ListPaymentInquiryResponseDto response = new ListPaymentInquiryResponseDto();
		String serviceResponse = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();

		try {

			// Log request in audit table
			paymentInquirylogAudit(request.getIssuerTrxRef(), CommonConstant.PAYMENT_INQUIRY_SERVICE, new Date(),
					CommonConstant.VOUCHERCODE, CommonConstant.INITIATE_REQUEST, CommonConstant.MESSAGE);

			// get last generated Token ID
			GimacAuthentication accessToken = transactionDao.getLastRecordByAccessTokenId();
			if (accessToken != null) {

				// get Config Details
				Map<String, String> configMap = systemConfigDao.getConfigDetailsMap();

				// set Headers
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + accessToken.getAccessToken());
				connectionRequest.setHeaders(headerMap);

				// Set Connection request.
				connectionRequest.setHttpmethodName("POST");
				connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
				connectionRequest.setServiceUrl(configMap.get(CommonConstant.BASE_URL)
						+ configMap.get(CommonConstant.PAYMENT) + "/" + configMap.get(CommonConstant.INQUIRY));
				// log detais in audit table
				paymentInquirylogAudit(request.getIssuerTrxRef(), CommonConstant.PAYMENT_INQUIRY_SERVICE, new Date(),
						CommonConstant.VOUCHERCODE, CommonConstant.INITIATE_REQUEST_TO_PARTNER, CommonConstant.MESSAGE);
				String req = gson.toJson(request);

				// eventLog
				eventRequestLog(req, request.getIssuerTrxRef());

				LOGGER.info("PaymentInquiryServiceImpl in paymentInquiryService function request " + req);
				// getting gimac response
				// serviceResponse = CallServices.getResponseFromService(connectionRequest,
				// req);
				LOGGER.info(
						"PaymentInquiryServiceImpl in paymentInquiryService function response : " + serviceResponse);

				// Dummy Response
				serviceResponse = "[\n" + " {\n" + " \"intent\":\"mobile_transfer\",\n"
						+ " \"voucherCode\":\"2gnc5ugSHUOQ\",\n" + " \"amount\":100,\n" + " \"currency\":\"950\",\n"
						+ " \"state\":\"ACCEPTED\",\n" + " \"description\":\"gift\",\n"
						+ " \"issuerTrxRef\":\"824717465280\",\n" + " \"toMember\":\"10008\",\n"
						+ " \"fromMember\":\"10025\",\n" + " \"createTime\":1536079612776,\n"
						+ " \"acquirerTrxRef\":\"824717465280\",\n" + " \"accountNumber\":\"0432820103022701\",\n"
						+ " \"senderMobile\":\"+6693625466998\",\n" + " \"validityDuration\":0,\n"
						+ " \"walletDestination\":\"102030405060\",\n" + " \"walletSource\":\"user1\"\n" + " },\n"
						+ " {\n" + " \"intent\":\"mobile_transfer\",\n" + " \"voucherCode\":\"O4DZThfG0Wbu\",\n"
						+ " \"amount\":1000,\n" + " \"currency\":\"950\",\n" + " \"state\":\"ACCEPTED\",\n"
						+ " \"description\":\"test\",\n" + " \"issuerTrxRef\":\"824716282323\",\n"
						+ " \"toMember\":\"10008\",\n" + " \"fromMember\":\"10025\",\n"
						+ " \"createTime\":1536074903204,\n" + " \"acquirerTrxRef\":\"824716282323\",\n"
						+ " \"accountNumber\":\"0432820103022701\",\n" + " \"senderMobile\":\"+6693625466998\",\n"
						+ " \"validityDuration\":0,\n" + " \"walletDestination\":\"102030405060\",\n"
						+ " \"walletSource\":\"user1\"\n" + " }\n" + "]";

				// log event response
				eventResponseLog(serviceResponse, request.getIssuerTrxRef(), CommonConstant.PAYMENT_INQUIRY_SERVICE);

				Type collectionType = new TypeToken<List<PaymentInquiryResponseDto>>() {
				}.getType();
				List<PaymentInquiryResponseDto> responseList = gson.fromJson(serviceResponse, collectionType);
				response.setPaymentInquiry(responseList);
				LOGGER.info(" PaymentInquiryServiceImpl in PaymentInquiryService function response " + response);

				// log Payment Inquiry request and response
				logResponse(request, response);

				if (response.equals(null)) {
					response = new ListPaymentInquiryResponseDto();
					response.setCode(MFSGimacCodes.ER201.getCode());
					response.setMessage(MFSGimacCodes.ER201.getMessage());
				}

			} else {
				response = new ListPaymentInquiryResponseDto();
				response.setCode(MFSGimacCodes.ER216.getCode());
				response.setMessage(MFSGimacCodes.ER216.getMessage());

			}

			// log detais in audit table
			paymentInquirylogAudit(request.getIssuerTrxRef(), CommonConstant.PAYMENT_INQUIRY_SERVICE, new Date(),
					CommonConstant.VOUCHERCODE, CommonConstant.RESPONSE_FROM_PARTNER, CommonConstant.MESSAGE);

		} catch (DaoException de) {

			LOGGER.error("==>DaoException in PaymentInquiryServiceImpl" + de);
			response = new ListPaymentInquiryResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());

		} catch (Exception e) {
			LOGGER.error("==>Exception in PaymentInquiryServiceImpl" + e);
			response = new ListPaymentInquiryResponseDto();
			response.setCode(MFSGimacCodes.ER207.getCode());
			response.setMessage(MFSGimacCodes.ER207.getMessage());

		}

		return response;
	}

	// log event response
	private void eventResponseLog(String response, String issuerTrxRef, String serviceName) throws DaoException {
		GimacEventLog transactionEventLog = transactionDao.getByIssureTrxRefAndServiceName(issuerTrxRef, serviceName);
		if (transactionEventLog != null) {
			transactionEventLog.setResponse(response);
			transactionEventLog.setDateLogged(new Date());
			transactionDao.update(transactionEventLog);

		}
	}

	// log event request
	private void eventRequestLog(String request, String issuerTrxRef) throws DaoException {

		GimacEventLog eventLog = new GimacEventLog();

		eventLog.setMfsId(issuerTrxRef);
		eventLog.setRequest(request);
		eventLog.setServiceName(CommonConstant.PAYMENT_INQUIRY_SERVICE);
		eventLog.setDateLogged(new Date());
		transactionDao.save(eventLog);

	}

	// log request and response
	private void logResponse(PaymentInquiryRequestDto request, ListPaymentInquiryResponseDto responseList)
			throws DaoException {

		GimacPaymentInquiry gimacPaymentInquiryRequest = transactionDao
				.getPaymentInquiryByIssuerTrxRef(request.getIssuerTrxRef());
		if (gimacPaymentInquiryRequest == null) {
			GimacPaymentInquiry gimacPaymentInquiryReq = new GimacPaymentInquiry();
			gimacPaymentInquiryReq.setAsIssuer(request.getAsIssuer());
			gimacPaymentInquiryReq.setAsAcquirer(request.getAsAcquirer());
			gimacPaymentInquiryReq.setFirstElement(request.getFirstElement());
			gimacPaymentInquiryReq.setPageSize(request.getPageSize());
			gimacPaymentInquiryReq.setIssuerTrxRef(request.getIssuerTrxRef());
			gimacPaymentInquiryReq.setAcquirerTrxRef(request.getAcquirerTrxRef());
			gimacPaymentInquiryReq.setVoucherCode(request.getVoucherCode());
			String status1 = request.getStatus().toString();
			String status = status1.substring(1, status1.length() - 1);
			gimacPaymentInquiryReq.setStatus(status);
			String intents1 = request.getIntents().toString();
			String intents = intents1.substring(1, intents1.length() - 1);
			gimacPaymentInquiryReq.setIntents(intents);
			gimacPaymentInquiryReq.setFromMember(request.getFromMember());
			gimacPaymentInquiryReq.setToMember(request.getToMember());
			gimacPaymentInquiryReq.setFromDate(request.getFromDate());
			gimacPaymentInquiryReq.setToDate(request.getToDate());
			transactionDao.save(gimacPaymentInquiryReq);
		} else {
			gimacPaymentInquiryRequest.setAsIssuer(request.getAsIssuer());
			gimacPaymentInquiryRequest.setAsAcquirer(request.getAsAcquirer());
			gimacPaymentInquiryRequest.setFirstElement(request.getFirstElement());
			gimacPaymentInquiryRequest.setPageSize(request.getPageSize());
			gimacPaymentInquiryRequest.setIssuerTrxRef(request.getIssuerTrxRef());
			gimacPaymentInquiryRequest.setAcquirerTrxRef(request.getAcquirerTrxRef());
			gimacPaymentInquiryRequest.setVoucherCode(request.getVoucherCode());
			String status1 = request.getStatus().toString();
			String status = status1.substring(1, status1.length() - 1);
			gimacPaymentInquiryRequest.setStatus(status);
			String intents1 = request.getIntents().toString();
			String intents = intents1.substring(1, intents1.length() - 1);
			gimacPaymentInquiryRequest.setIntents(intents);
			gimacPaymentInquiryRequest.setFromMember(request.getFromMember());
			gimacPaymentInquiryRequest.setToMember(request.getToMember());
			gimacPaymentInquiryRequest.setFromDate(request.getFromDate());
			gimacPaymentInquiryRequest.setToDate(request.getToDate());
			transactionDao.update(gimacPaymentInquiryRequest);
		}

		List<PaymentInquiryResponseDto> list = responseList.getPaymentInquiry();
		if (list.size() != 0) {
			for (PaymentInquiryResponseDto paymentList : list) {

				if (paymentList != null) {
					String issuerTrxRef = paymentList.getIssuerTrxRef();
					if (issuerTrxRef != null) {
						GimacPaymentInquiry gimacPaymentInquiry = transactionDao
								.getPaymentInquiryByIssuerTrxRef(paymentList.getIssuerTrxRef());

						if (gimacPaymentInquiry == null) {
							GimacPaymentInquiry gimacPaymentInquiry2 = new GimacPaymentInquiry();
							gimacPaymentInquiry2.setAsIssuer(request.getAsIssuer());
							gimacPaymentInquiry2.setAsAcquirer(request.getAsAcquirer());
							gimacPaymentInquiry2.setFirstElement(request.getFirstElement());
							gimacPaymentInquiry2.setPageSize(request.getPageSize());
							gimacPaymentInquiry2.setIssuerTrxRef(request.getIssuerTrxRef());
							gimacPaymentInquiry2.setAcquirerTrxRef(request.getAcquirerTrxRef());
							gimacPaymentInquiry2.setVoucherCode(request.getVoucherCode());
							String status1 = request.getStatus().toString();
							String status = status1.substring(1, status1.length() - 1);
							gimacPaymentInquiry2.setStatus(status);
							String intents1 = request.getIntents().toString();
							String intents = intents1.substring(1, intents1.length() - 1);
							gimacPaymentInquiry2.setIntents(intents);
							gimacPaymentInquiry2.setIntent(paymentList.getIntent());
							gimacPaymentInquiry2.setVoucherCode(paymentList.getVoucherCode());
							gimacPaymentInquiry2.setAmount(paymentList.getAmount());
							gimacPaymentInquiry2.setCurrency(paymentList.getCurrency());
							gimacPaymentInquiry2.setState(paymentList.getState());
							gimacPaymentInquiry2.setDescription(paymentList.getDescription());
							gimacPaymentInquiry2.setIssuerTrxRef(paymentList.getIssuerTrxRef());
							gimacPaymentInquiry2.setFromMember(paymentList.getFromMember());
							gimacPaymentInquiry2.setToMember(paymentList.getToMember());
							gimacPaymentInquiry2.setCreateTime(paymentList.getCreateTime());
							gimacPaymentInquiry2.setAcquirerTrxRef(paymentList.getAcquirerTrxRef());
							gimacPaymentInquiry2.setAccountNumber(paymentList.getAccountNumber());
							gimacPaymentInquiry2.setSenderMobile(paymentList.getSenderMobile());
							gimacPaymentInquiry2.setValidityDuration(paymentList.getValidityDuration());
							gimacPaymentInquiry2.setWalletDestination(paymentList.getWalletDestination());
							gimacPaymentInquiry2.setWalletSource(paymentList.getWalletSource());
							gimacPaymentInquiry2.setDateLogged(new Date());
							transactionDao.save(gimacPaymentInquiry2);
						} else {
							gimacPaymentInquiry.setIntent(paymentList.getIntent());
							gimacPaymentInquiry.setVoucherCode(paymentList.getVoucherCode());
							gimacPaymentInquiry.setAmount(paymentList.getAmount());
							gimacPaymentInquiry.setCurrency(paymentList.getCurrency());
							gimacPaymentInquiry.setState(paymentList.getState());
							gimacPaymentInquiry.setDescription(paymentList.getDescription());
							gimacPaymentInquiry.setIssuerTrxRef(paymentList.getIssuerTrxRef());
							gimacPaymentInquiry.setToMember(paymentList.getToMember());
							gimacPaymentInquiry.setFromMember(paymentList.getFromMember());
							gimacPaymentInquiry.setCreateTime(paymentList.getCreateTime());
							gimacPaymentInquiry.setAcquirerTrxRef(paymentList.getAcquirerTrxRef());
							gimacPaymentInquiry.setAccountNumber(paymentList.getAccountNumber());
							gimacPaymentInquiry.setSenderMobile(paymentList.getSenderMobile());
							gimacPaymentInquiry.setValidityDuration(paymentList.getValidityDuration());
							gimacPaymentInquiry.setWalletDestination(paymentList.getWalletDestination());
							gimacPaymentInquiry.setWalletSource(paymentList.getWalletSource());
							gimacPaymentInquiry.setDateLogged(new Date());
							transactionDao.update(gimacPaymentInquiry);
						}
					}
				}
			}
		}
	}
}